This is the readme file for our final project at Telerik Academy. We were given
the task to develop a virtual wallet management system that offered the
following functionalities:

User registration and login
Creation of different wallets and cards.
Adding funds to your wallet via bank-provided API
Creation of transaction between different users
Searching and sorting
Admin panel with admin having extra functionalities to block and unblock users
as well as having access to all users and transactions.

We implemented the following optional tasks:
1) Email verification for users. If a user is unverified he or she cannot
make transactions. We encountered the following issue - if our local firewall 
and antivirus program is turned on - the program cannot send an email
2) Support for multiple currencies - BGN, EUR and USD with different set 
exchange rates
3) Support for a user having multiple wallets and cards
4) Easter egg
5) Photo validation of users - a user has to submit a selfie and an id photo
in order to create transactions. An admin can see all unverified users and check
their submitted photos and choose whether to verify a user. In both cases 
the photos are deleted in order to adhere to GDPR standarts.
6) We have hosted our project online at: 
https://virtualwalletscalefocus.herokuapp.com/ + hosted a DB at AWS and
connected them successfully.

What was not implemented:
We did not manage to implement proper filtration.

We used a JPA repository and attempted to adhere to the SOLID principles. We
tried to limit having data logic in our controllers and have adequate
exception handling and displaying proper error messages to our users. 

Main issues that we faced:
Pagination took us way too much time to implement.
We started with Hibernate and no DTOs are implemented them around a week and a
half before the deadline. We had to do a lot of refactoring. If we had to do it
all over again - we'd start with a default JPA Repository and implement DTOs

What can be improved:
The UI can definitely be improved
Fix error handling to display more meaningful messages
Refactor the controllers that contain business logic
Improve unit testing
