package virtualwalletproject.main;

import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.UserDtoForCreation;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.models.Wallet;

public class Factory {
    public static User createUser() {
        return new User("testUser", "test@test.bg", "0885110842");
    }

    public static UserDtoForCreation createUserDTO() {
        return new UserDtoForCreation("testUser", "test@test.bg", "0885110842");
    }

    public static Card createCard(){
        return new Card("testtesttesttest");
    }

    public static Wallet createWallet() {
        return new Wallet(1, "testName");
    }

}

