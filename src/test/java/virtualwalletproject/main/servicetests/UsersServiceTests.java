package virtualwalletproject.main.servicetests;

import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.models.ConfirmationToken;
import com.virtualwalletproject.main.models.DTOs.UserDtoForCreation;
import com.virtualwalletproject.main.models.DTOs.UserDtoForEdit;
import com.virtualwalletproject.main.models.DTOs.UserDtoForVerification;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForCreation;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.models.Wallet;
import com.virtualwalletproject.main.repositories.*;
import com.virtualwalletproject.main.services.contracts.TransactionsService;
import com.virtualwalletproject.main.services.contracts.WalletsService;
import com.virtualwalletproject.main.services.implementation.EmailSenderService;
import com.virtualwalletproject.main.services.implementation.UsersServiceImpl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static virtualwalletproject.main.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceTests {
    @Mock
    private UsersRepository usersRepository;
    @InjectMocks
    private UsersServiceImpl usersService;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    EmailSenderService emailSenderService;
    @Mock
    UserDetailsManager userDetailsManager;
    @Mock
    TransactionsService transactionsService;
    @Mock
    WalletsService walletsService;
    @Mock
    ConfirmationTokenRepository confirmationTokenRepository;
    @Mock
    WalletsRepository walletsRepository;

    @Test
    public void createShouldThrowWhenUserExists() {
        UserDtoForCreation userDtoForCreation = createUserDTO();
        Mockito.when(usersService.checkUserExistsByUsername(anyString())).thenReturn(true);
        Assertions.assertThrows(DuplicateEntityException.class, () -> usersService.createUser(userDtoForCreation));
    }

    @Test
    public void createShouldCreateUser() {
        UserDtoForCreation user1 = createUserDTO();
        WalletDtoForCreation walletDtoForCreation = new WalletDtoForCreation();
        User user = createUser();
        Wallet wallet = createWallet();
        List<Wallet> wallets = new ArrayList<>();
        wallets.add(wallet);
        user.setPassword("testtest");
        user.setWallets(wallets);
        Mockito.when(usersService.checkUserExistsByUsername(anyString())).thenReturn(false);
        Mockito.when(usersRepository.getByUsername(anyString())).thenReturn(user);
        Mockito.when(passwordEncoder.encode(any())).thenReturn(anyString());
        Mockito.when(walletsRepository.getWalletsByUser(user.getUsername())).thenReturn(wallets);
        Mockito.when(walletsService.getWalletByName(anyString())).thenReturn(wallet);
        walletsService.createWallet(walletDtoForCreation, user1.getUsername());
        Mockito.when(walletsService.getWalletsByUsername(Mockito.anyString()))
                .thenReturn(wallets);
        user.setDefaultWallet(wallet);
        usersService.createUser(user1);
        Mockito.verify(usersRepository, Mockito.times(1)).save(user);

    }

    @Test
    public void updateShouldCallRepo() {
        UserDtoForEdit user = new UserDtoForEdit();
        User user1 = createUser();
        user.setPhoneNumber("0855555");
        user.setFirstName("test");
        user.setLastName("tst");
        user1.setPhoneNumber(user.getPhoneNumber());
        user1.setPhoneNumber(user.getFirstName());
        user1.setLastName(user.getLastName());
        Mockito.when(usersRepository.getByUsername(user1.getUsername())).thenReturn(user1);
        usersService.updateUser(user1.getUsername(), user);
    }

    @Test
    public void updateShouldCallRepo2() {
        UserDtoForVerification user = new UserDtoForVerification();
        User user1 = createUser();
        user.setPersonalIdCardPhoto("test");
        user.setSelfiePhoto("test");
        user1.setPersonalIdCardPhoto(user.getPersonalIdCardPhoto());
        user1.setSelfiePhoto(user.getSelfiePhoto());
        Mockito.when(usersRepository.getByUsername(user1.getUsername())).thenReturn(user1);
        usersService.updateUser(user1.getUsername(), user);
    }

    @Test
    public void getByUserNameShouldReturnProperUser() {
        User user = createUser();
        Mockito.when(usersService.checkUserExistsByUsername(anyString())).thenReturn(false);
        Mockito.when(usersRepository.getByUsername(anyString())).thenReturn(user);
        User expectedUser = usersService.getUserByUsername(user.getUsername());
        Assert.assertEquals(user, expectedUser);
    }

    @Test
    public void deleteShouldDeleteUser() {
        User user = createUser();
        Mockito.when(usersService.checkUserExistsByUsername(anyString())).thenReturn(true);
        usersService.deleteUser(user);
    }

    @Test
    public void getAllUsers() {
        Pageable pageable = Pageable.unpaged();
        User user = createUser();
        List<User> users = new ArrayList<>();
        users.add(user);
        usersService.getAllUsers(pageable);
        Mockito.verify(usersRepository, Mockito.times(1)).findAllBy(pageable);
    }

    @Test
    public void getAllUnverified() {
        Pageable pageable = Pageable.unpaged();
        User user = createUser();
        user.setVerifiedStatus(false);
        List<User> users = new ArrayList<>();
        users.add(user);
        usersService.getAllUnverifiedUsers(pageable);
        Mockito.verify(usersRepository, Mockito.times(1)).findAllByVerifiedStatusIsFalse(pageable);
    }
    @Test
    public void confirmShouldChangeStatus(){
        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setConfirmationToken("testete");
        User user = createUser();
        confirmationToken.setUser(user);
        Mockito.when(confirmationTokenRepository.existsByConfirmationToken(confirmationToken.getConfirmationToken())).thenReturn(true);
        Mockito.when(confirmationTokenRepository.findByConfirmationToken(anyString())).thenReturn(confirmationToken);
        Mockito.when(usersRepository.getByUsername(confirmationToken.getUser().getUsername())).thenReturn(user);
        usersService.confirmUser(confirmationToken.getConfirmationToken());
        Mockito.verify(usersRepository, Mockito.times(1)).save(user);
    }
}


