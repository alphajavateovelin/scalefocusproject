package virtualwalletproject.main.servicetests;

import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForCreation;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForEdit;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.models.Wallet;
import com.virtualwalletproject.main.repositories.UsersRepository;
import com.virtualwalletproject.main.repositories.WalletsRepository;
import com.virtualwalletproject.main.services.contracts.DtoMapper;
import com.virtualwalletproject.main.services.implementation.WalletsServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import org.springframework.data.domain.Pageable;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.*;
import static virtualwalletproject.main.Factory.createUser;
import static virtualwalletproject.main.Factory.createWallet;

@RunWith(MockitoJUnitRunner.class)
public class WalletsServiceTests {
    @Mock
    WalletsRepository walletsRepository;
    @Mock
    UsersRepository usersRepository;
    @Mock
    DtoMapper dtoMapper;

    @InjectMocks
    private WalletsServiceImpl walletsService;

    @Test
    public void createShouldThrowWhenWalletExists() {
        User user = createUser();
        WalletDtoForCreation walletDtoForCreation = new WalletDtoForCreation();
        Wallet wallet = createWallet();
        wallet.setUsers(new ArrayList<>());
        user.setWallets(new ArrayList<>());
        wallet.getUsers().add(user);
        user.getWallets().add(wallet);
        Mockito.when(dtoMapper.toWallet(walletDtoForCreation)).thenReturn(wallet);
        Mockito.when(usersRepository.getByUsername(user.getUsername())).thenReturn(user);
        Assertions.assertThrows(DuplicateEntityException.class, () -> walletsService.createWallet(walletDtoForCreation, user.getUsername()));
    }

    @Test
    public void createShouldCreateWallet() {
        User user = createUser();
        WalletDtoForCreation walletDtoForCreation = new WalletDtoForCreation();
        Wallet wallet = createWallet();
        wallet.setUsers(new ArrayList<>());
        user.setWallets(new ArrayList<>());
        Mockito.when(dtoMapper.toWallet(walletDtoForCreation)).thenReturn(wallet);
        Mockito.when(usersRepository.getByUsername(anyString())).thenReturn(user);
        walletsService.createWallet(walletDtoForCreation, user.getUsername());
        Mockito.verify(walletsRepository, Mockito.times(1)).save(wallet);
        Mockito.verify(usersRepository, Mockito.times(1)).save(user);
    }

    @Test
    public void updateShouldCallRepo() {
        Wallet wallet = createWallet();
        Mockito.when(walletsRepository.getById(anyInt())).thenReturn(wallet);
        WalletDtoForEdit walletDtoForEdit = new WalletDtoForEdit();
        walletsService.updateWallet(walletDtoForEdit, wallet.getId());
        Mockito.verify(walletsRepository, Mockito.times(1)).save(wallet);
    }

    @Test
    public void getWalletShouldGetWallet(){
        Wallet wallet = createWallet();
        Mockito.when(walletsRepository.getById(anyInt())).thenReturn(wallet);
        Mockito.when(walletsService.getWalletById(anyInt())).thenReturn(wallet);
        Wallet expectedWallet = walletsService.getWalletById(wallet.getId());
        Assert.assertEquals(wallet, expectedWallet);
    }
    @Test
    public void getWalletByNameShouldGetWallet(){
        Wallet wallet = createWallet();
        Mockito.when(walletsRepository.getByName(anyString())).thenReturn(wallet);
        Mockito.when(walletsService.getWalletByName(anyString())).thenReturn(wallet);
        Wallet expectedWallet = walletsService.getWalletByName(wallet.getName());
        Assert.assertEquals(wallet, expectedWallet);
    }

    @Test
    public void deleteWalletShouldDeleteWallet(){
        Wallet wallet = createWallet();
        Mockito.when(walletsRepository.getById(wallet.getId())).thenReturn(wallet);
        walletsService.deleteWallet(wallet.getId());
        Mockito.verify(walletsRepository, Mockito.times(1)).delete(wallet);
    }


}
