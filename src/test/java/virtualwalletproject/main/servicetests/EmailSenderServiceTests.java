package virtualwalletproject.main.servicetests;

import com.virtualwalletproject.main.services.implementation.EmailSenderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@RunWith(MockitoJUnitRunner.class)
public class EmailSenderServiceTests {
    @InjectMocks
    private EmailSenderService emailSenderService;
    @Mock
    private JavaMailSender javaMailSender;

    @Test
    public void emailServiceShouldSendEmail(){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        emailSenderService.sendEmail(mailMessage);
        Mockito.verify(javaMailSender, Mockito.times(1)).send(mailMessage);
    }
}
