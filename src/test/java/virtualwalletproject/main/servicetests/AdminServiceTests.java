package virtualwalletproject.main.servicetests;


import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.repositories.UsersRepository;
import com.virtualwalletproject.main.services.implementation.AdminServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static virtualwalletproject.main.Factory.createUser;

@RunWith(MockitoJUnitRunner.class)
public class AdminServiceTests {
    @Mock
    private UsersRepository usersRepository;
    @InjectMocks
    private AdminServiceImpl adminService;

    @Test
    public void blockShouldBlock(){
        User user = createUser();
        user.setBlockedStatus(false);
        Mockito.when(usersRepository.getByUsername(anyString())).thenReturn(user);
        adminService.block(user.getUsername());
        Assert.assertTrue(user.isBlockedStatus());
    }
    @Test
    public void verifyShouldVerify(){
        User user = createUser();
        user.setVerifiedStatus(false);
        Mockito.when(usersRepository.getByUsername(anyString())).thenReturn(user);
        adminService.verify(user.getUsername());
        Assert.assertTrue(user.isVerifiedStatus());
    }
}
