package virtualwalletproject.main.servicetests;

import com.virtualwalletproject.main.models.*;
import com.virtualwalletproject.main.models.DTOs.TransactionFundDto;
import com.virtualwalletproject.main.models.DTOs.TransactionTransferDto;
import com.virtualwalletproject.main.models.enums.Status;
import com.virtualwalletproject.main.repositories.TransactionsRepository;
import com.virtualwalletproject.main.repositories.UsersRepository;
import com.virtualwalletproject.main.repositories.WalletsRepository;
import com.virtualwalletproject.main.services.PaymentProcessor.PaymentProcessorServiceImpl;
import com.virtualwalletproject.main.services.contracts.CardsService;
import com.virtualwalletproject.main.services.contracts.PaymentProcessorService;
import com.virtualwalletproject.main.services.contracts.UsersService;
import com.virtualwalletproject.main.services.contracts.WalletsService;
import com.virtualwalletproject.main.services.implementation.CardsServiceImpl;
import com.virtualwalletproject.main.services.implementation.TransactionsServiceImpl;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.Null;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static virtualwalletproject.main.Factory.*;


@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTests {
    @Mock
    TransactionsRepository transactionsRepository;
    @Mock
    WalletsService walletsService;
    @Mock
    UsersRepository usersRepository;
    @Mock
    UsersService usersService;
    @Mock
    WalletsRepository walletsRepository;
    @Mock
    PaymentProcessorService paymentProcessorService;
    @Mock
    CardsService cardsService;
    @InjectMocks
    TransactionsServiceImpl transactionsService;
    @Test
    public void getAllTransactions(){
        Transaction transaction = new OutgoingTransfer();
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.add(transaction);
        Page<Transaction> transactions = new PageImpl(transactionList);
        transactionsService.getAllTransactions(Pageable.unpaged());
        Mockito.verify(transactionsRepository, Mockito.times(1)).findAll(Pageable.unpaged());
    }

    @Test
    public void getAllTransactionsByUserName(){
        User user = createUser();
        Transaction transaction = new OutgoingTransfer();
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.add(transaction);
        Page<Transaction> transactions = new PageImpl(transactionList);
        transactionsService.getAllTransactionsByUsername(user.getUsername(), Pageable.unpaged());
        Mockito.verify(transactionsRepository, Mockito.times(1)).findAllByUserUsername(user.getUsername(),Pageable.unpaged());
    }
    @Test
    public void getAllTransactionsByAmount(){
        int amount = 10;
        Transaction transaction = new OutgoingTransfer();
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.add(transaction);
        Page<Transaction> transactions = new PageImpl(transactionList);
        Mockito.when(transactionsService.findAllByAmount(anyDouble(), Mockito.any(Pageable.class))).thenReturn(transactions);
        transactionsService.findAllByAmount(amount, Pageable.unpaged());
        Mockito.verify(transactionsRepository, Mockito.times(1)).findAllByAmount(amount, Pageable.unpaged());
    }

    @Test
    public void findAllByDate(){
        Date date = new Date();
        Transaction transaction = new OutgoingTransfer();
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.add(transaction);
        Page<Transaction> transactions = new PageImpl(transactionList);
        Mockito.when(transactionsService.findAllByDate(Mockito.any(Date.class), Mockito.any(Pageable.class))).thenReturn(transactions);
        transactionsService.findAllByDate(date, Pageable.unpaged());
        Mockito.verify(transactionsRepository, Mockito.times(1)).findAllByDate(date, Pageable.unpaged());
    }

    @Test
    public void getAllByWallet(){
       Wallet wallet = createWallet();
        Transaction transaction = new OutgoingTransfer();
        List<Transaction> transactionList = new ArrayList<>();
        transactionList.add(transaction);
        Page<Transaction> transactions = new PageImpl(transactionList);
        Mockito.when(transactionsRepository.findAllByWallet(Mockito.any(Wallet.class), Mockito.any(Pageable.class ))).thenReturn(transactions);
        transactionsService.findAllByWallet(wallet, Pageable.unpaged());
        Mockito.verify(transactionsRepository, Mockito.times(1)).findAllByWallet(wallet, Pageable.unpaged());
    }

    @Test
    public void createTransactionShouldCreateTransaction(){
        User sender = createUser();
        sender.setWallets(new ArrayList<>());
        User received = createUser();
        received.setUsername("test2");
        Wallet receiverWallet = createWallet();
        received.setDefaultWallet(receiverWallet);
        Wallet wallet = createWallet();
        wallet.setBalance(BigDecimal.valueOf(100000));
        sender.getWallets().add(wallet);
        TransactionTransferDto transactionTransferDto = new TransactionTransferDto();
        transactionTransferDto.setAmount(BigDecimal.valueOf(100));
        Transaction transaction = new OutgoingTransfer(transactionTransferDto.getAmount(), LocalDateTime.now(), wallet, received.getDefaultWallet(), sender);
        Transaction transaction1 = new IncomingTransfer(transactionTransferDto.getAmount().multiply(BigDecimal.valueOf(-1)), LocalDateTime.now(),received.getDefaultWallet(), wallet, received);
        Mockito.when(walletsService.getWalletById(transactionTransferDto.getSenderWalletId())).thenReturn(wallet);
        Mockito.when(usersService.getUserByUsername(sender.getUsername())).thenReturn(sender);
        Mockito.when(usersService.getUserByUsername(transactionTransferDto.getReceiverUsername())).thenReturn(received);
        transactionsService.createTransaction(transactionTransferDto, sender.getUsername());
        transactionsRepository.save(transaction);
        transactionsRepository.save(transaction1);
    }

    @Test
    public void addFundTransactionTest() throws IOException {
        PaymentResponse paymentResponse = new PaymentResponse(Status.SUCCESSFUL, "success");
       Card card = createCard();
       Wallet wallet = createWallet();
       User user = createUser();
       Currency currency = new Currency();
        TransactionFundDto dto = new TransactionFundDto();
        dto.setAmount(BigDecimal.valueOf(100));
        wallet.setUsers(new ArrayList<>());
        wallet.getUsers().add(user);
        card.setUser(user);
        Mockito.when(walletsService.getWalletById(anyInt())).thenReturn(wallet);
        Mockito.when(usersService.getUserByUsername(user.getUsername())).thenReturn(user);
        Mockito.when(cardsService.getCardByCardNumber(dto.getCardNumber())).thenReturn(card);
        Mockito.when(paymentProcessorService.processCardTransaction(any(Card.class), BigDecimal.valueOf(anyLong()))).thenReturn(paymentResponse);
        PaymentResponse outcome = paymentProcessorService.processCardTransaction(card, dto.getAmount().multiply(BigDecimal.valueOf(100)));
        transactionsService.createTransaction(dto, user.getUsername());

    }






}
