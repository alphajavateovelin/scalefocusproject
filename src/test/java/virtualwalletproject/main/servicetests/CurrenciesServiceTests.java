package virtualwalletproject.main.servicetests;

import com.virtualwalletproject.main.models.Currency;
import com.virtualwalletproject.main.repositories.CurrenciesRepository;
import com.virtualwalletproject.main.services.implementation.CurrenciesServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CurrenciesServiceTests {
    @Mock
    CurrenciesRepository currenciesRepository;
    @InjectMocks
    CurrenciesServiceImpl currenciesService;
@Test
    public void getAllShouldGetAllCurrencies(){
        currenciesService.getAllCurrencies();
        Mockito.verify(currenciesRepository, Mockito.times(1)).findAllBy();
    }
    @Test
    public void getCurrencyByIdShouldReturnCurrency(){
    Currency currency = new Currency();
    Mockito.when(currenciesRepository.getById(anyInt())).thenReturn(currency);
    Currency expectedCurrency = currenciesService.getCurrencyById(currency.getId());
        Assert.assertEquals(currency, expectedCurrency);
    }
}
