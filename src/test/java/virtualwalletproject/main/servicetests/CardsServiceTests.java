package virtualwalletproject.main.servicetests;

import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.CardDtoForCreationAndEdit;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.repositories.CardsRepository;
import com.virtualwalletproject.main.services.contracts.CurrenciesService;
import com.virtualwalletproject.main.services.contracts.DtoMapper;
import com.virtualwalletproject.main.services.contracts.UsersService;
import com.virtualwalletproject.main.services.implementation.CardsServiceImpl;
import com.virtualwalletproject.main.services.implementation.DtoMapperImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Pageable;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static virtualwalletproject.main.Factory.createCard;
import static virtualwalletproject.main.Factory.createUser;

@RunWith(MockitoJUnitRunner.class)
public class CardsServiceTests {
    @Mock
    CardsRepository cardsRepository;
    @Mock
    Pageable pageable;
    @Mock
    UsersService usersService;
    @InjectMocks
    CardsServiceImpl cardsService;
    @Mock
    CurrenciesService currenciesService;
    @Mock
    DtoMapper dtoMapper;
    @Mock
    Card newCard;

    @Test
    public void getById_Should_ReturnCard() {
        Card card = new Card();
        Mockito.when(cardsRepository.getCardByIdAndDeletedIsFalse(anyInt())).thenReturn(card);
        Card expectedCard = cardsService.getCardById(anyInt());
        Assert.assertSame(expectedCard, card);
    }

    @Test
    public void getAllCards_ShouldReturnAllCards() {
        User user = createUser();
        Pageable pageable = Pageable.unpaged();
        Mockito.when(usersService.getUserByUsername(anyString())).thenReturn(user);
        cardsService.getCardsByUsername(user.getUsername());
        Mockito.verify(cardsRepository, Mockito.times(1)).findAllByUserUsernameAndDeletedIsFalse(user.getUsername());
    }

    @Test
    public void createShouldThrow() {
        User user = createUser();
        DtoMapper dtoMapper = new DtoMapperImpl(currenciesService);
        CardDtoForCreationAndEdit cardDtoForCreationAndEdit = new CardDtoForCreationAndEdit("1234123412341234");
        Card card = dtoMapper.toCard(cardDtoForCreationAndEdit);
        Mockito.when(usersService.getUserByUsername(anyString())).thenReturn(user);
        Assertions.assertThrows(NullPointerException.class, () -> cardsService.createCard(cardDtoForCreationAndEdit, user.getUsername()));
    }

    @Test
    public void DeleteShouldCallRepo() {
        Card card = createCard();
        Mockito.when(cardsRepository.getCardByCardNumberAndDeletedIsFalse(anyString())).thenReturn(card);
        Mockito.when(cardsService.getCardByCardNumber(anyString())).thenReturn(card);
        cardsService.deleteCard(card.getId());
        Mockito.verify(cardsRepository, Mockito.times(1)).delete(card);
    }

    @Test
    public void getByNameShouldGetCard() {
        Card card = createCard();
        Mockito.when(cardsRepository.getCardByCardNumberAndDeletedIsFalse(anyString())).thenReturn(card);
        Card expectedCard = cardsService.getCardByCardNumber(card.getCardNumber());
        Assert.assertEquals(card, expectedCard);
    }

    @Test
    public void getByIdShouldGetCard() {
        Card card = createCard();
        Mockito.when(cardsRepository.getCardByIdAndDeletedIsFalse(anyInt())).thenReturn(card);
        Card expectedCard = cardsService.getCardById(card.getId());
        Assert.assertEquals(card, expectedCard);
    }

    @Test
    public void updateCardShouldCallRep() {
        User user = createUser();
        CardDtoForCreationAndEdit cardDtoForCreationAndEdit = new CardDtoForCreationAndEdit();
        Card card = createCard();
        Mockito.when(cardsRepository.getCardByIdAndDeletedIsFalse(anyInt())).thenReturn(card);
        card.setCurrency(currenciesService.getCurrencyById(1));
        card.setUser(user);
        cardDtoForCreationAndEdit.setCardNumber("1234123412341234");
        cardDtoForCreationAndEdit.setCardHolder(user.getUsername());
        cardDtoForCreationAndEdit.setCvv(111);
        cardDtoForCreationAndEdit.setExpirationDate("10/20");
        cardsService.updateCard(card.getId(), cardDtoForCreationAndEdit, user.getUsername());
        Mockito.verify(cardsRepository, Mockito.times(1)).save(card);
    }



    @Test
    public void checkCardById(){
        Card card = createCard();
        Assert.assertFalse(cardsService.checkCardExistsById(card.getId()));
    }

    @Test
    public void checkCardByNumber(){
        Card card = createCard();
        Assert.assertFalse(cardsService.checkCardExistsByCardNumber(card.getCardNumber()));
    }


}
