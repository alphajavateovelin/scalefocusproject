package com.virtualwalletproject.main.models.DTOs;

import com.virtualwalletproject.main.constants.AppConstants;

import javax.validation.constraints.Size;

public class WalletDtoForEdit {

    @Size(min = AppConstants.WALLET_NAME_MIN_LENGTH, max = AppConstants.WALLET_NAME_MAX_LENGTH, message = AppConstants.WALLET_NAME_LENGTH_ERROR_MESSAGE)
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
