package com.virtualwalletproject.main.models.DTOs;

public class UserDtoForVerification {
    private String selfiePhoto;
    private String personalIdCardPhoto;

    public UserDtoForVerification() {
    }

    public String getSelfiePhoto() {
        return selfiePhoto;
    }

    public void setSelfiePhoto(String selfiePhoto) {
        this.selfiePhoto = selfiePhoto;
    }

    public String getPersonalIdCardPhoto() {
        return personalIdCardPhoto;
    }

    public void setPersonalIdCardPhoto(String personalIdCardPhoto) {
        this.personalIdCardPhoto = personalIdCardPhoto;
    }
}
