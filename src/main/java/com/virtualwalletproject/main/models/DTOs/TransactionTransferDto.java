package com.virtualwalletproject.main.models.DTOs;

import java.math.BigDecimal;

public class TransactionTransferDto {
    private BigDecimal amount;
    private String receiverUsername;
    private int senderWalletId;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReceiverUsername() {
        return receiverUsername;
    }

    public void setReceiverUsername(String receiverUsername) {
        this.receiverUsername = receiverUsername;
    }

    public int getSenderWalletId() {
        return senderWalletId;
    }

    public void setSenderWalletId(int senderWalletId) {
        this.senderWalletId = senderWalletId;
    }
}
