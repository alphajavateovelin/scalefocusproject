package com.virtualwalletproject.main.models.DTOs;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class WalletDtoForPresentation {

    private String name;

    private BigDecimal balance;

    private int currencyId;

    private List<String> usernames;

    public WalletDtoForPresentation(String name, BigDecimal balance, int currencyId, List<String> usernames) {
        this.name = name;
        this.balance = balance;
        this.currencyId = currencyId;
        this.usernames = usernames;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public void setUsernames(List<String> usernames) {
        this.usernames = usernames;
    }
}
