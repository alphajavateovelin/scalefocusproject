package com.virtualwalletproject.main.models.DTOs;

import com.virtualwalletproject.main.constants.AppConstants;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDtoForEdit {
    private String password;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;
    @Email(message = AppConstants.EMAIL_ERROR_MESSAGE)
    @NotNull
    private String email;
    @NotNull
    @Size(min = AppConstants.PHONE_LENGTH, max = AppConstants.PHONE_LENGTH, message = AppConstants.PHONE_LENGTH_ERROR_MESSAGE)
    private String phoneNumber;
    private String picture;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
