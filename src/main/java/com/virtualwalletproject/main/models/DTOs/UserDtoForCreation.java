package com.virtualwalletproject.main.models.DTOs;

import com.virtualwalletproject.main.constants.AppConstants;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class UserDtoForCreation {
    @NotNull
    @Size(min =AppConstants.USERNAME_MIN_LENGTH, max = AppConstants.USERNAME_MAX_LENGTH, message = AppConstants.USERNAME_LENGTH_ERROR_MESSAGE)
    private String username;
    @NotNull
    @NotEmpty(message = AppConstants.PASSWORD_ERROR_MESSAGE)
    @Size(max=AppConstants.PASSWORD_MAX_LENGTH, min=AppConstants.PASSWORD_MIN_LENGTH, message = AppConstants.PASSWORD_ERROR_MESSAGE)
    private String password;
    private String firstName;
    private String lastName;
    @Email(message = AppConstants.EMAIL_ERROR_MESSAGE)
    @NotEmpty(message = AppConstants.EMAIL_MUST_NOT_BE_EMPTY)
    private String email;
    @NotNull
    @Size(min = AppConstants.PHONE_LENGTH, max = AppConstants.PHONE_LENGTH, message = AppConstants.PHONE_LENGTH_ERROR_MESSAGE)
    private String phoneNumber;
    private String picture;

    public UserDtoForCreation(String testUser, String s, String s1) {
        this.username = testUser;
        this.email = s;
        this.phoneNumber = s1;
    }

    public UserDtoForCreation() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
