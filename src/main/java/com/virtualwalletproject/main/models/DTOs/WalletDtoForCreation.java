package com.virtualwalletproject.main.models.DTOs;

import com.virtualwalletproject.main.constants.AppConstants;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class WalletDtoForCreation {

    @Size(min = AppConstants.WALLET_NAME_MIN_LENGTH, max = AppConstants.WALLET_NAME_MAX_LENGTH, message = AppConstants.WALLET_NAME_LENGTH_ERROR_MESSAGE)
    private String name;
    @NotNull
    @Positive
    private int currencyId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
