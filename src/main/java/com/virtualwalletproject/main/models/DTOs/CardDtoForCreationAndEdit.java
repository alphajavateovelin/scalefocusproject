package com.virtualwalletproject.main.models.DTOs;

import com.virtualwalletproject.main.constants.AppConstants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CardDtoForCreationAndEdit {
    @NotNull
//    @Size(min= AppConstants.CARD_NUMBER_LENGTH, max = AppConstants.CARD_NUMBER_LENGTH, message=AppConstants.CARD_NUMBER_LENGTH_ERROR)
    private String cardNumber;
    @NotNull
    @Size(min=AppConstants.EXP_DATE_LENGTH, max = AppConstants.EXP_DATE_LENGTH, message = AppConstants.EXP_DATE_ERROR)
    private String expirationDate;
    @NotNull
    private int cvv;
    @NotNull
    @Size(min = AppConstants.USERNAME_MIN_LENGTH, max = AppConstants.USERNAME_MAX_LENGTH, message = AppConstants.USERNAME_LENGTH_ERROR_MESSAGE)
    private String cardHolder;
    @NotNull
    private int currencyId;

    public CardDtoForCreationAndEdit(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public CardDtoForCreationAndEdit() {
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }
}
