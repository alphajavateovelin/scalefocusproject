package com.virtualwalletproject.main.models;

import com.virtualwalletproject.main.models.enums.Status;
import com.virtualwalletproject.main.models.enums.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@DiscriminatorValue(value = Type.Values.outgoingTransfer)
public class OutgoingTransfer extends Transaction{

    @ManyToOne
    @JoinColumn(name = "ReceiverWallet_id")
    private Wallet receiver;

    public OutgoingTransfer() {
        super(BigDecimal.ZERO, LocalDateTime.now(), Status.SUCCESSFUL, Type.OutgoingTransfer, null, null);
    }

    @Override
    public Wallet getCounterPart() {
        return receiver;
    }

    public OutgoingTransfer(BigDecimal amount, LocalDateTime date, Wallet wallet, Wallet receiver, User user) {
        super(amount, date, Status.SUCCESSFUL, Type.OutgoingTransfer, wallet, user);
        this.receiver = receiver;
    }

    public Wallet getReceiver() {
        return receiver;
    }

    public void setReceiver(Wallet receiver) {
        this.receiver = receiver;
    }
}
