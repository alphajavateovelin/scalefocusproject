package com.virtualwalletproject.main.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phoneNumber;

    @Column(name = "photo")
    @Lob
    private String picture;

    @Column(name = "selfiePhoto")
    @Lob
    private String selfiePhoto;

    @Column(name="personalIdCardPhoto")
    @Lob
    private String personalIdCardPhoto;

    @Column(name = "isBlocked")
    private boolean blockedStatus;
    @Column(name = "isVerified")
    private boolean verifiedStatus;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_wallets",
            joinColumns = @JoinColumn(name = "username", referencedColumnName = "username"),
            inverseJoinColumns = @JoinColumn(name = "Wallets_id", referencedColumnName = "id")
    )
    private List<Wallet> wallets;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "defaultWallet")
    private Wallet defaultWallet;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Card> cards;

    @Column(name = "isConfirmed")
    private boolean isConfirmed;

    public User() {
        this.wallets = new ArrayList<>();
        this.cards = new ArrayList<>();
    }

    public User(String username, String email, String phoneNumber){
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public boolean isConfirmed() {
        return isConfirmed;
    }

    public void setConfirmed(boolean confirmed) {
        isConfirmed = confirmed;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public List<Wallet> getWallets() {
        return wallets;
    }

    public void setWallets(List<Wallet> wallets) {
        this.wallets = wallets;
    }

    public Wallet getDefaultWallet() {
        return defaultWallet;
    }

    public void setDefaultWallet(Wallet defaultWallet) {
        this.defaultWallet = defaultWallet;
    }

    public Boolean isBlockedStatus() {
        return blockedStatus;
    }

    public void setBlockedStatus(Boolean blockedStatus) {
        this.blockedStatus = blockedStatus;
    }


    public void setVerifiedStatus(Boolean verifiedStatus) {
        this.verifiedStatus = verifiedStatus;
    }

    public String getSelfiePhoto() {
        return selfiePhoto;
    }

    public void setSelfiePhoto(String selfiePhoto) {
        this.selfiePhoto = selfiePhoto;
    }

    public String getPersonalIdCardPhoto() {
        return personalIdCardPhoto;
    }

    public void setPersonalIdCardPhoto(String personalIdCardPhoto) {
        this.personalIdCardPhoto = personalIdCardPhoto;
    }

    public Boolean isVerifiedStatus() {
        return verifiedStatus;
    }
}
