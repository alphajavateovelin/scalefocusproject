package com.virtualwalletproject.main.models;

import javax.persistence.*;

@Entity
@Table(name = "currencies")
public class Currency {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "shortCode")
    private String shortCode;

    @Column(name = "rateToBase")
    private float rateToBase;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public float getRateToBase() {
        return rateToBase;
    }

    public void setRateToBase(float rateToBase) {
        this.rateToBase = rateToBase;
    }
}
