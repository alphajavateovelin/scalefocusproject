package com.virtualwalletproject.main.models;

import com.virtualwalletproject.main.models.enums.Status;
import com.virtualwalletproject.main.models.enums.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@DiscriminatorValue(value = Type.Values.fund)
public class Fund extends Transaction {

    @ManyToOne
    @JoinColumn(name = "Cards_id")
    private Card card;

    public Fund() {
        super(BigDecimal.ZERO, LocalDateTime.now(), Status.SUCCESSFUL, Type.Fund, null, null);
    }

    @Override
    public Card getCounterPart() {
        return card;
    }

    public Fund(BigDecimal amount, LocalDateTime date, Wallet wallet, Card card, User user) {
        super(amount, date, Status.SUCCESSFUL, Type.Fund, wallet, user);
        this.card = card;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }
}
