package com.virtualwalletproject.main.models.enums;

public enum Status {
    SUCCESSFUL(Values.successful),
    PENDING(Values.pending),
    FAILED(Values.failed),
    TERMINATED(Values.terminated);

    Status(String value) {
        // force equality between name of enum instance, and value of constant
        if (!this.name().equals(value))
            throw new IllegalArgumentException("Incorrect use of Status");
    }

    public static class Values {
        public static final String successful = "SUCCESSFUL";
        public static final String pending = "PENDING";
        public static final String failed = "FAILED";
        public static final String terminated = "TERMINATED";
    }
}
