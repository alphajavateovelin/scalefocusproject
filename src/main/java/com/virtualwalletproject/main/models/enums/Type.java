package com.virtualwalletproject.main.models.enums;

public enum Type {
    Fund(Values.fund),
    IncomingTransfer(Values.incomingTransfer),
    OutgoingTransfer(Values.outgoingTransfer),
    Payment(Values.payment);

    Type(String value) {
        // force equality between name of enum instance, and value of constant
        if (!this.name().equals(value))
            throw new IllegalArgumentException("Incorrect use of Type");
    }

    public static class Values {
        public static final String fund = "Fund";
        public static final String incomingTransfer = "IncomingTransfer";
        public static final String outgoingTransfer = "OutgoingTransfer";
        public static final String payment = "Payment";
    }
}
