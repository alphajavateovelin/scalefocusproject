package com.virtualwalletproject.main.models;

import com.virtualwalletproject.main.models.enums.Status;
import com.virtualwalletproject.main.models.enums.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@DiscriminatorValue(value = Type.Values.incomingTransfer)
public class IncomingTransfer extends Transaction{

    @ManyToOne
    @JoinColumn(name = "SenderWallet_id")
    private Wallet sender;

    public IncomingTransfer() {
        super(BigDecimal.ZERO, LocalDateTime.now(), Status.SUCCESSFUL, Type.IncomingTransfer, null, null);
    }

    @Override
    public Wallet getCounterPart() {
        return sender;
    }

    public IncomingTransfer(BigDecimal amount, LocalDateTime date, Wallet wallet, Wallet sender, User user) {
        super(amount, date, Status.SUCCESSFUL, Type.IncomingTransfer, wallet, user);
        this.sender = sender;
    }

    public Wallet getSender() {
        return sender;
    }

    public void setSender(Wallet sender) {
        this.sender = sender;
    }
}
