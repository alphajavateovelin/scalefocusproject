package com.virtualwalletproject.main.models;

public class EasterEgg {
    public String answer;

    public EasterEgg() {
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
