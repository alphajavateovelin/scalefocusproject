package com.virtualwalletproject.main.models;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "wallets")
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "currentBalance")
    private BigDecimal balance;

    @OneToMany(mappedBy = "wallet")
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<Transaction> transactions;

    @ManyToOne
    @JoinColumn(name = "Currencies_id")
    private Currency currency;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_wallets",
            joinColumns = @JoinColumn(name = "Wallets_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "username", referencedColumnName = "username")
    )
    private List<User> users;

    public Wallet() {
        transactions = new ArrayList<>();
        users = new ArrayList<>();
    }

    public Wallet(int id, String testName) {
        this.id=id;
        this.name=testName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return name;
    }
}
