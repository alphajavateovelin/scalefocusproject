package com.virtualwalletproject.main.models;

import com.virtualwalletproject.main.models.enums.Status;

public class PaymentResponse {

    private Status status;

    private String message;

    public PaymentResponse(Status status, String message) {
        this.status = status;
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

}
