package com.virtualwalletproject.main.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    int id;

    @Column(name = "cardNumber")
    private String cardNumber;

    @Column(name = "expirationDate")
    private String expirationDate;

    @Column(name = "cvv")
    private int cvv;

    @Column(name = "cardHolder")
    private String cardHolder;

    @ManyToOne
    @JoinColumn(name = "Currencies_id")
    private Currency currency;

    @ManyToOne
    @JoinColumn(name = "username")
    private User user;

    @JoinColumn(name = "isDeleted")
    private boolean isDeleted;

    public Card() {
    }

    public Card(String cardNumber) {
        this.cardNumber = cardNumber;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    @Override
    public String toString() {
        return cardNumber;
    }
}