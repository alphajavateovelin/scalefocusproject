package com.virtualwalletproject.main.services.implementation;

import com.virtualwalletproject.main.constants.AppConstants;
import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.exceptions.ForbiddenActionException;
import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.CardDtoForCreationAndEdit;
import com.virtualwalletproject.main.models.DTOs.CardDtoForPresentation;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.repositories.CardsRepository;
import com.virtualwalletproject.main.services.contracts.CardsService;
import com.virtualwalletproject.main.services.contracts.CurrenciesService;
import com.virtualwalletproject.main.services.contracts.DtoMapper;
import com.virtualwalletproject.main.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardsServiceImpl implements CardsService {
    private CardsRepository cardsRepository;
    private UsersService usersService;
    private CurrenciesService currenciesService;
    private DtoMapper dtoMapper;

    @Autowired
    public CardsServiceImpl(CardsRepository cardsRepository, UsersService usersService, CurrenciesService currenciesService, DtoMapper dtoMapper) {
        this.cardsRepository = cardsRepository;
        this.usersService = usersService;
        this.currenciesService = currenciesService;
        this.dtoMapper = dtoMapper;
    }

    @Override
    public boolean checkCardExistsByCardNumber (String cardNumber) {
        return cardsRepository.getCardByCardNumberAndDeletedIsFalse(cardNumber)!=null;
    }

    @Override
    public boolean checkCardExistsById (int cardId) {
        return cardsRepository.existsById(cardId);
    }

    @Override
    public void createCard(CardDtoForCreationAndEdit card, String username) {
        User user = usersService.getUserByUsername(username);
        if (user==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        if (card==null){
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }
        Card newCard = dtoMapper.toCard(card);
        String cardNumberToEdit = card.getCardNumber().replaceAll("[^0-9]","");
        cardNumberToEdit = cardNumberToEdit.substring(0,4)+"-"+cardNumberToEdit.substring(4,8)+"-"+cardNumberToEdit.substring(8,12)+"-"+cardNumberToEdit.substring(12,16);
        if (cardsRepository.getCardByCardNumberAndDeletedIsFalse(cardNumberToEdit)!=null) {
            throw new DuplicateEntityException(AppConstants.CARD_ALREADY_EXISTS);
        }
        newCard.setCardNumber(cardNumberToEdit);
        newCard.setUser(user);
        cardsRepository.save(newCard);
    }

    @Override
    public void updateCard(int cardId, CardDtoForCreationAndEdit cardDto, String username) {
        if(cardDto==null){
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }
        Card card = cardsRepository.getCardByIdAndDeletedIsFalse(cardId);
        if (card == null) {
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }
        if (!card.getUser().getUsername().equals(username)) {
            throw new ForbiddenActionException(AppConstants.ACCESS_DENIED_ERROR_MESSAGE);
        }
        String cardNumberToEdit = cardDto.getCardNumber().replaceAll("[^0-9]","");
        cardNumberToEdit = cardNumberToEdit.substring(0,4)+"-"+cardNumberToEdit.substring(4,8)+"-"+cardNumberToEdit.substring(8,12)+"-"+cardNumberToEdit.substring(12,16);
        if (cardsRepository.getCardByCardNumberAndDeletedIsFalse(cardNumberToEdit) != null && cardsRepository.getCardByCardNumberAndDeletedIsFalse(cardNumberToEdit).getId()!=cardId) {
            throw new DuplicateEntityException(AppConstants.CARD_ALREADY_EXISTS);
        }
        card.setCardNumber(cardNumberToEdit);
        card.setCardHolder(cardDto.getCardHolder());
        card.setCvv(cardDto.getCvv());
        card.setExpirationDate(cardDto.getExpirationDate());
        card.setCurrency(currenciesService.getCurrencyById(cardDto.getCurrencyId()));
        cardsRepository.save(card);
    }

    @Override
    public void deleteCard(int cardId) {
        Card cardToDelete = cardsRepository.getCardByIdAndDeletedIsFalse(cardId);
        if (cardToDelete==null){
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }
        cardToDelete.setDeleted(true);
        cardsRepository.save(cardToDelete);
    }

    @Override
    public Card getCardByCardNumber(String cardNumber) {
        Card card = cardsRepository.getCardByCardNumberAndDeletedIsFalse(cardNumber);
        if (card==null){
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }
        return card;
    }

    @Override
    public Card getCardById(int cardId) {
        Card card = cardsRepository.getCardByIdAndDeletedIsFalse(cardId);
        if (card==null){
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }
        return card;
    }

    @Override
    public List<Card> getCardsByUsername(String username) {
        User user = usersService.getUserByUsername(username);
        if (user==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        return cardsRepository.findAllByUserUsernameAndDeletedIsFalse(username);
    }

    @Override
    public CardDtoForCreationAndEdit getCardDtoForEditByCardId(int cardId, String username) {
        Card card = cardsRepository.getCardByIdAndDeletedIsFalse(cardId);
        if (card == null) {
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }
        if(!card.getUser().getUsername().equals(username)) {
            throw new ForbiddenActionException(AppConstants.ACCESS_DENIED_ERROR_MESSAGE);
        }
        return dtoMapper.fromCard(card);
    }

    @Override
    public List<CardDtoForPresentation> getCardDtosForPresentationByUsername(String username) {
        //TODO
        return null;
    }
}