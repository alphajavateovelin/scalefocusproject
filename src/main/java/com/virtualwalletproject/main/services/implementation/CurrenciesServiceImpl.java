package com.virtualwalletproject.main.services.implementation;

import com.virtualwalletproject.main.constants.AppConstants;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.Currency;
import com.virtualwalletproject.main.repositories.CurrenciesRepository;
import com.virtualwalletproject.main.services.contracts.CurrenciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CurrenciesServiceImpl implements CurrenciesService {
    CurrenciesRepository currenciesRepository;
@Autowired
    public CurrenciesServiceImpl(CurrenciesRepository currenciesRepository) {
        this.currenciesRepository = currenciesRepository;
    }

    @Override
    public List<Currency> getAllCurrencies() {
        return currenciesRepository.findAllBy();
    }

    @Override
    public Currency getCurrencyById(int id) {
        Currency currency = currenciesRepository.getById(id);
        if (currency==null){
            throw new EntityNotFoundException(AppConstants.CURRENCY_NOT_FOUND);
        }
        return currency;
    }
}
