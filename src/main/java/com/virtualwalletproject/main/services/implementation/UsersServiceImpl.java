package com.virtualwalletproject.main.services.implementation;

import com.virtualwalletproject.main.constants.AppConstants;
import com.virtualwalletproject.main.exceptions.ForbiddenActionException;
import com.virtualwalletproject.main.models.ConfirmationToken;
import com.virtualwalletproject.main.models.DTOs.*;
import com.virtualwalletproject.main.repositories.ConfirmationTokenRepository;
import com.virtualwalletproject.main.repositories.UsersRepository;
import com.virtualwalletproject.main.services.contracts.DtoMapper;
import com.virtualwalletproject.main.services.contracts.UsersService;
import com.virtualwalletproject.main.services.contracts.WalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.User;

import java.util.List;
import java.util.Set;

@Service
public class UsersServiceImpl implements UsersService {
    private UsersRepository usersRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private ConfirmationTokenRepository confirmationTokenRepository;
    private EmailSenderService emailSenderService;
    private DtoMapper dtoMapper;
    private WalletsService walletsService;

    @Autowired
    public UsersServiceImpl(EmailSenderService emailSenderService, ConfirmationTokenRepository confirmationTokenRepository, UsersRepository usersRepository, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, DtoMapper dtoMapper, WalletsService walletsService) {
        this.usersRepository = usersRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.confirmationTokenRepository = confirmationTokenRepository;
        this.emailSenderService = emailSenderService;
        this.dtoMapper = dtoMapper;
        this.walletsService = walletsService;
    }

    @Override
    public void createUser(UserDtoForCreation userDto) {

        userDto.setUsername(userDto.getUsername().trim());

        if(userDto.getUsername().length()<AppConstants.USERNAME_MIN_LENGTH||userDto.getUsername().length()>AppConstants.USERNAME_MAX_LENGTH){ //TODO unnecessary validation here, should be done with DTO annotations
            throw new ForbiddenActionException(AppConstants.USERNAME_LENGTH_ERROR_MESSAGE);
        }

        if (checkUserExistsByUsername(userDto.getUsername())) {
            throw new DuplicateEntityException(AppConstants.USER_ALREADY_EXISTS);
        }
        if (checkUserExistsByEmail(userDto.getEmail())){
            throw new DuplicateEntityException(AppConstants.EMAIL_ALREADY_EXISTS);
        }
        if (checkUserExistsByPhoneNumber(userDto.getPhoneNumber())){
            throw new DuplicateEntityException(AppConstants.PHONE_ALREADY_EXISTS);
        }

        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User securityUser =
                new org.springframework.security.core.userdetails.User(
                userDto.getUsername(),
                passwordEncoder.encode(userDto.getPassword()),
                authorities);
        userDetailsManager.createUser(securityUser);

        User newUser = getUserByUsername(userDto.getUsername());
        newUser.setFirstName(userDto.getFirstName());
        newUser.setLastName(userDto.getLastName());
        newUser.setEmail(userDto.getEmail());
        newUser.setPhoneNumber(userDto.getPhoneNumber());

        WalletDtoForCreation walletDto = new WalletDtoForCreation();
        walletDto.setName("Main Wallet");
        walletDto.setCurrencyId(1);
        walletsService.createWallet(walletDto, userDto.getUsername());
        newUser.setDefaultWallet(walletsService.getWalletsByUsername(userDto.getUsername()).get(0));

        usersRepository.save(newUser);
        emailSender(newUser);
    }

    public void emailSender(User user) {
        ConfirmationToken confirmationToken = new ConfirmationToken(user);
        confirmationTokenRepository.save(confirmationToken);
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(user.getEmail());
        simpleMailMessage.setFrom(AppConstants.EMAIL_ADDRESS);
        simpleMailMessage.setSubject(AppConstants.EMAIL_SUBJECT);
        simpleMailMessage.setText(AppConstants.EMAIL_CONFIRM_TEXT + "http://localhost:8080/confirm-account?token=" + confirmationToken.getConfirmationToken());
        emailSenderService.sendEmail(simpleMailMessage);
    }

    @Override
    public User getUserByUsername(String username) {
        User user = usersRepository.getByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        return user;
    }

    @Override
    public Page<User> getAllUsers(Pageable pageable) {
        return usersRepository.findAllBy(pageable);
    }

    @Override
    public boolean checkUserExistsByUsername(String username) {
        return usersRepository.existsByUsername(username);
    }

    @Override
    public boolean checkUserExistsByEmail(String email) {
        return usersRepository.existsByEmail(email);
    }

    @Override
    public boolean checkUserExistsByPhoneNumber(String phone) {
        return usersRepository.existsByPhoneNumber(phone);
    }

    @Override
    public void updateUser(String username, UserDtoForEdit userDto) {
        User userToUpdate = getUserByUsername(username);
        userToUpdate.setFirstName(userDto.getFirstName());
        userToUpdate.setLastName(userDto.getLastName());
        userToUpdate.setEmail(userDto.getEmail());
        userToUpdate.setPhoneNumber(userDto.getPhoneNumber());
        if(userDto.getPassword() != null && !userDto.getPassword().isEmpty()) {
            userToUpdate.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }
        if (userDto.getPicture() != null && !userDto.getPicture().isEmpty()) {
            userToUpdate.setPicture(userDto.getPicture());
        }
        usersRepository.save(userToUpdate);
    }

    @Override
    public void updateUser(String username, UserDtoForVerification userDto) {
        User userToUpdate = getUserByUsername(username);
        if (userToUpdate==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        userToUpdate.setSelfiePhoto(userDto.getSelfiePhoto());
        userToUpdate.setPersonalIdCardPhoto(userDto.getPersonalIdCardPhoto());
        usersRepository.save(userToUpdate);
    }

    @Override
    public void deleteUser(User user) {
        if (user ==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        usersRepository.delete(user);
    }

    @Override
    public Page<User> getAllUnverifiedUsers(Pageable pageable) {
        return usersRepository.findAllByVerifiedStatusIsFalse(pageable);

    }

    @Override
    public UserDtoForPresentationDetailed getDetailedUserDtoForPresentationByUsername(String username) {
        User user = getUserByUsername(username);
        if (user ==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        return dtoMapper.fromUserToDetailedDtoForPresentation(user);
    }

    @Override
    public UserDtoForVerification getUserDtoForVerificationByUsername(String username) {
        User user = getUserByUsername(username);
        if (user ==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        return dtoMapper.fromUserToVerificationDto(user);
    }

    @Override
    public boolean confirmUser (String token) {
        if(confirmationTokenRepository.existsByConfirmationToken(token)){
            ConfirmationToken confirmationToken = confirmationTokenRepository.findByConfirmationToken(token);
            User user = getUserByUsername(confirmationToken.getUser().getUsername());
            user.setConfirmed(true);
            usersRepository.save(user);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Page<User> getAllUserDtosForPresentationBrief(Pageable pageable) {
        return usersRepository.findAll(pageable);
    }

    @Override
    public List<String> search(String keyword) {
        return usersRepository.searchForUsersByUsername(keyword);
    }

    @Override
    public Page<User> getAllByQuery(String username, String email, String phoneNumber, Pageable pageable) {

        return usersRepository.getAllUserWithQuery(username, email, phoneNumber, pageable);
    }
}
