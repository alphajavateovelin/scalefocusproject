package com.virtualwalletproject.main.services.implementation;

import com.virtualwalletproject.main.constants.AppConstants;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.repositories.UsersRepository;
import com.virtualwalletproject.main.services.contracts.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {
    private UsersRepository usersRepository;

    @Autowired
    public AdminServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

        @Override
        public void block(String userName) {
        User userToBeBlocked = usersRepository.getByUsername(userName);
        if (userToBeBlocked==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
            if(userToBeBlocked.isBlockedStatus()){
                userToBeBlocked.setBlockedStatus(false);
            } else {
                userToBeBlocked.setBlockedStatus(true);
            }
            usersRepository.save(userToBeBlocked);
        }

    @Override
    public void verify(String userName) {
        User userToBeVerified = usersRepository.getByUsername(userName);
        if (userToBeVerified==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        if(!userToBeVerified.isVerifiedStatus()){
            userToBeVerified.setVerifiedStatus(true);
            userToBeVerified.setPersonalIdCardPhoto(null);
            userToBeVerified.setSelfiePhoto(null);
        }
        usersRepository.save(userToBeVerified);
    }

}
