package com.virtualwalletproject.main.services.implementation;

import com.virtualwalletproject.main.constants.AppConstants;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.exceptions.ForbiddenActionException;
import com.virtualwalletproject.main.exceptions.InsufficientBalanceException;
import com.virtualwalletproject.main.models.*;
import com.virtualwalletproject.main.models.DTOs.TransactionFundDto;
import com.virtualwalletproject.main.models.DTOs.TransactionTransferDto;
import com.virtualwalletproject.main.models.enums.Status;
import com.virtualwalletproject.main.models.enums.Type;
import com.virtualwalletproject.main.repositories.*;
import com.virtualwalletproject.main.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class TransactionsServiceImpl implements TransactionsService {
    private TransactionsRepository transactionsRepository;
    private WalletsRepository walletsRepository;
    private PaymentProcessorService paymentProcessorService;
    private CardsService cardsService;
    private WalletsService walletsService;
    private UsersService usersService;
    private CurrenciesService currenciesService;


    @Autowired
    public TransactionsServiceImpl(TransactionsRepository transactionsRepository, WalletsRepository walletsRepository, PaymentProcessorService paymentProcessorService, CardsService cardsService, WalletsService walletsService, UsersService usersService, CurrenciesService currenciesService) {
        this.transactionsRepository = transactionsRepository;
        this.paymentProcessorService = paymentProcessorService;
        this.cardsService = cardsService;
        this.walletsService = walletsService;
        this.usersService = usersService;
        this.currenciesService = currenciesService;
        this.walletsRepository = walletsRepository;
    }

    @Override
    public void createTransaction(TransactionTransferDto transactionTransferDto, String senderUsername) {

        User receiver = usersService.getUserByUsername(transactionTransferDto.getReceiverUsername());
        if (receiver == null) {
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }

        User sender = usersService.getUserByUsername(senderUsername);
        Wallet senderWallet = walletsService.getWalletById(transactionTransferDto.getSenderWalletId());

        if (transactionTransferDto.getAmount().compareTo(senderWallet.getBalance()) > 0) {
            throw new ForbiddenActionException(AppConstants.INVALID_AMOUNT);
        } else {
            float receiverRate = receiver.getDefaultWallet().getCurrency().getRateToBase();
            float senderRate = senderWallet.getCurrency().getRateToBase();
            BigDecimal sendAmount = transactionTransferDto.getAmount().multiply(BigDecimal.valueOf(receiverRate).divide(BigDecimal.valueOf(senderRate), 2, RoundingMode.HALF_UP));

            senderWallet.setBalance(senderWallet.getBalance().subtract(transactionTransferDto.getAmount()));
            receiver.getDefaultWallet().setBalance(receiver.getDefaultWallet().getBalance().add(sendAmount));

            Transaction outgoingTransfer = new OutgoingTransfer(transactionTransferDto.getAmount(), LocalDateTime.now(), senderWallet, receiver.getDefaultWallet(), sender);
            Transaction incomingTransfer = new IncomingTransfer(BigDecimal.valueOf(-1).multiply(sendAmount), LocalDateTime.now(), receiver.getDefaultWallet(), senderWallet, receiver);

            transactionsRepository.save(outgoingTransfer);
            walletsRepository.save(senderWallet);
            transactionsRepository.save(incomingTransfer);
            walletsRepository.save(receiver.getDefaultWallet());
            //TODO should be in SQL transaction
        }
    }

    @Override
    public void createTransaction(TransactionFundDto transactionFundDto, String username) throws IOException {

        Card card = cardsService.getCardByCardNumber(transactionFundDto.getCardNumber());
        if (card == null) {
            throw new EntityNotFoundException(AppConstants.CARD_NOT_FOUND);
        }

        if (!card.getUser().getUsername().equals(username)) {
            throw new ForbiddenActionException(AppConstants.TRANSACTION_ERROR_MESSAGE);
        }

        Wallet wallet = walletsService.getWalletById(transactionFundDto.getWalletId());
        if (wallet == null) {
            throw new EntityNotFoundException(AppConstants.WALLET_NOT_FOUND);
        }

        User user = usersService.getUserByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        if (!wallet.getUsers().contains(user)) {
            throw new ForbiddenActionException(AppConstants.INVALID_RECEIVER_ERROR_MESSAGE);
        }

        float cardRate = card.getCurrency().getRateToBase();
        float walletRate = wallet.getCurrency().getRateToBase();
        BigDecimal sendAmount = transactionFundDto.getAmount().multiply(BigDecimal.valueOf(cardRate).divide(BigDecimal.valueOf(walletRate), 2, RoundingMode.HALF_UP));

        String idempotencyKey = UUID.randomUUID().toString();

        PaymentResponse outcome = paymentProcessorService.processCardTransaction(card, sendAmount.multiply(BigDecimal.valueOf(100)), idempotencyKey);
        if (outcome.getStatus().equals(Status.SUCCESSFUL)) {
            wallet.setBalance(wallet.getBalance().add(sendAmount));
            Transaction fund = new Fund(sendAmount, LocalDateTime.now(), wallet, card, user);
            transactionsRepository.save(fund);
            walletsRepository.save(wallet);
            //TODO should be in SQL transaction
        }
        if (outcome.getStatus().equals(Status.FAILED)) {
            throw new InsufficientBalanceException(AppConstants.STATUS_FAILED_ERROR_MESSAGE);
        }
        if (outcome.getStatus().equals(Status.TERMINATED)) {
            throw new ForbiddenActionException(AppConstants.STATUS_TERMINATED_ERROR_MESSAGE);
        }
    }

    @Override
    public Page<Transaction> getAllTransactions(Pageable pageable) {
        return transactionsRepository.findAll(pageable);
    }

    @Override
    public Page<Transaction> getAllTransactionsByUsername(String username, Pageable pageable) {
        return transactionsRepository.findAllByUserUsername(username, pageable);
    }


    @Override
    public Page<Transaction> findAllByAmount(double amount, Pageable pageable) {
        return transactionsRepository.findAllByAmount(amount, pageable);
    }

    @Override
    public Page<Transaction> findAllByDate(Date date, Pageable pageable) {
        return transactionsRepository.findAllByDate(date, pageable);
    }

    @Override
    public Page<Transaction> findAllByWallet(Wallet wallet, Pageable pageable) {
        if (wallet == null) {
            throw new EntityNotFoundException(AppConstants.WALLET_NOT_FOUND);
        }
        return transactionsRepository.findAllByWallet(wallet, pageable);
    }

    @Override
    public Page<Transaction> findAllByType(Type type, Pageable pageable) {
        return transactionsRepository.findAllByType(type, pageable);
    }

}