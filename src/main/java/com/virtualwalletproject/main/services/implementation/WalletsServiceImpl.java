package com.virtualwalletproject.main.services.implementation;

import com.virtualwalletproject.main.constants.AppConstants;
import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForCreation;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForEdit;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForPresentation;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.models.Wallet;
import com.virtualwalletproject.main.repositories.UsersRepository;
import com.virtualwalletproject.main.repositories.WalletsRepository;
import com.virtualwalletproject.main.services.contracts.CurrenciesService;
import com.virtualwalletproject.main.services.contracts.DtoMapper;
import com.virtualwalletproject.main.services.contracts.UsersService;
import com.virtualwalletproject.main.services.contracts.WalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Service
public class WalletsServiceImpl implements WalletsService {
    private WalletsRepository walletsRepository;
    private UsersRepository usersRepository;
    private CurrenciesService currenciesService;
    private DtoMapper dtoMapper;

    @Autowired
    public WalletsServiceImpl(WalletsRepository walletsRepository, UsersRepository usersRepository, CurrenciesService currenciesService, DtoMapper dtoMapper) {
        this.walletsRepository = walletsRepository;
        this.usersRepository = usersRepository;
        this.currenciesService = currenciesService;
        this.dtoMapper = dtoMapper;
    }

    @Override
    public void createWallet(WalletDtoForCreation walletDto, String username) {
        Wallet wallet = dtoMapper.toWallet(walletDto);
        if (wallet==null){
            throw new EntityNotFoundException(AppConstants.WALLET_NOT_FOUND);
        }
        User user = usersRepository.getByUsername(username);
        if (user==null){
            throw new EntityNotFoundException(AppConstants.USER_NOT_FOUND);
        }
        wallet.setBalance(new BigDecimal(BigInteger.ZERO,2));
        wallet.getUsers().add(user);
        if(user.getWallets().contains(wallet)){
            throw new DuplicateEntityException(AppConstants.WALLET_ALREADY_EXISTS_IN_USER);
        }

        walletsRepository.save(wallet);
    }

    @Override
    public void updateWallet(WalletDtoForEdit wallet, int walletId) {
        Wallet walletToUpdate = walletsRepository.getById(walletId);
        if (walletToUpdate==null){
            throw new EntityNotFoundException(AppConstants.WALLET_NOT_FOUND);
        }
        walletToUpdate.setName(wallet.getName());
        walletsRepository.save(walletToUpdate);
    }

    @Override
    public void deleteWallet(int id) {
        Wallet wallet = walletsRepository.getById(id);
        if (wallet==null){
            throw new EntityNotFoundException(AppConstants.WALLET_NOT_FOUND);
        }
        walletsRepository.delete(wallet);
    }



    @Override
    public Wallet getWalletByName(String name) {
        Wallet wallet=walletsRepository.getByName(name);
        if (wallet==null){
            throw new EntityNotFoundException(AppConstants.WALLET_NOT_FOUND);
        }
        return wallet;
    }



    @Override
    public Wallet getWalletById(int id) {
        Wallet wallet=walletsRepository.getById(id);
        if(wallet==null){
            throw new EntityNotFoundException(AppConstants.WALLET_NOT_FOUND);
        }
        return wallet;
    }

    @Override
    public List<Wallet> getWalletsByUsername(String username) {
        return walletsRepository.getWalletsByUser(username);
    }

    @Override
    public Page<WalletDtoForPresentation> getWalletDtosForPresentationByUsername(String username, Pageable pageable) {
        //TODO
        return null;
    }
}
