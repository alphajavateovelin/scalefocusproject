package com.virtualwalletproject.main.services.implementation;

import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.*;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.models.Wallet;
import com.virtualwalletproject.main.services.contracts.CurrenciesService;
import com.virtualwalletproject.main.services.contracts.DtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DtoMapperImpl implements DtoMapper {
    CurrenciesService currenciesService;

    public DtoMapperImpl() {
    }

    @Autowired
    public DtoMapperImpl(CurrenciesService currenciesService) {
        this.currenciesService = currenciesService;
    }

    @Override
    public Wallet toWallet (WalletDtoForCreation wallet) {
        Wallet newWallet = new Wallet();
        newWallet.setName(wallet.getName());
        newWallet.setCurrency(currenciesService.getCurrencyById(wallet.getCurrencyId()));
        return newWallet;
    }

    @Override
    public WalletDtoForPresentation fromWalletToDtoForPresentation (Wallet wallet) {
        List<String> usernames = new ArrayList<>();
        for (User user:
             wallet.getUsers()) {
            usernames.add(user.getUsername());
        }
        return new WalletDtoForPresentation(wallet.getName(), wallet.getBalance(), wallet.getCurrency().getId(), usernames);
    }

    @Override
    public Card toCard (CardDtoForCreationAndEdit card) {
        Card newCard = new Card();
        newCard.setCardNumber(card.getCardNumber());
        newCard.setCardHolder(card.getCardHolder());
        newCard.setCurrency(currenciesService.getCurrencyById(card.getCurrencyId()));
        newCard.setCvv(card.getCvv());
        newCard.setExpirationDate(card.getExpirationDate());
        newCard.setDeleted(false);
        return newCard;
    }

    @Override
    public CardDtoForCreationAndEdit fromCard(Card card) {
        CardDtoForCreationAndEdit cardDto = new CardDtoForCreationAndEdit();
        cardDto.setCardNumber(card.getCardNumber());
        cardDto.setCardHolder(card.getCardHolder());
        cardDto.setCurrencyId(card.getCurrency().getId());
        cardDto.setCvv(card.getCvv());
        cardDto.setExpirationDate(card.getExpirationDate());
        return cardDto;
    }

    public UserDtoForPresentationDetailed fromUserToDetailedDtoForPresentation (User user) {
        UserDtoForPresentationDetailed userDto = new UserDtoForPresentationDetailed();
        userDto.setUsername(user.getUsername());
        userDto.setFirstName(user.getFirstName());
        userDto.setLastName(user.getLastName());
        userDto.setEmail(user.getEmail());
        userDto.setPhoneNumber(user.getPhoneNumber());
        userDto.setVerificationStatus(user.isVerifiedStatus());
        userDto.setPicture(user.getPicture());
        return userDto;
    }

    @Override
    public UserDtoForVerification fromUserToVerificationDto(User user) {
        UserDtoForVerification userDto = new UserDtoForVerification();
        userDto.setPersonalIdCardPhoto(user.getPersonalIdCardPhoto());
        userDto.setSelfiePhoto(user.getSelfiePhoto());
        return userDto;
    }

}
