package com.virtualwalletproject.main.services.contracts;

public interface AdminService {
    void block(String userName);
    void verify(String userName);
}
