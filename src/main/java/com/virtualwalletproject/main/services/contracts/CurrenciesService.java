package com.virtualwalletproject.main.services.contracts;

import com.virtualwalletproject.main.models.Currency;

import java.util.List;

public interface CurrenciesService {

    List<Currency> getAllCurrencies();

    Currency getCurrencyById(int id);
}
