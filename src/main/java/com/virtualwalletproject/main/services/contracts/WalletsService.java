package com.virtualwalletproject.main.services.contracts;

import com.virtualwalletproject.main.models.DTOs.WalletDtoForCreation;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForEdit;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForPresentation;
import com.virtualwalletproject.main.models.Wallet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;


public interface WalletsService {
    void createWallet(WalletDtoForCreation wallet, String username);
    void updateWallet(WalletDtoForEdit wallet, int walletId);
    void deleteWallet(int id);
    Wallet getWalletByName(String name);
    Wallet getWalletById(int id);
    List<Wallet> getWalletsByUsername(String username);
    Page<WalletDtoForPresentation> getWalletDtosForPresentationByUsername (String username, Pageable pageable);
}
