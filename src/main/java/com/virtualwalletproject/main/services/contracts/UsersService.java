package com.virtualwalletproject.main.services.contracts;

import com.virtualwalletproject.main.models.DTOs.*;
import com.virtualwalletproject.main.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UsersService {

    void createUser(UserDtoForCreation user);

    User getUserByUsername(String username);
    UserDtoForPresentationDetailed getDetailedUserDtoForPresentationByUsername (String username);
    UserDtoForVerification getUserDtoForVerificationByUsername (String username);

    Page<User> getAllUsers(Pageable pageable);
    Page<User> getAllUserDtosForPresentationBrief(Pageable pageable);

    boolean checkUserExistsByUsername(String username);
    boolean checkUserExistsByEmail(String email);
    boolean checkUserExistsByPhoneNumber(String phone);

    void updateUser(String username, UserDtoForEdit userDto);
    void updateUser(String username, UserDtoForVerification userDto);
    void deleteUser(User user);

    Page<User> getAllUnverifiedUsers(Pageable pageable);

    boolean confirmUser (String token);

    List<String> search(String keyword);

    Page<User> getAllByQuery(String username, String email, String phoneNumber, Pageable pageable);
}
