package com.virtualwalletproject.main.services.contracts;

import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.CardDtoForCreationAndEdit;
import com.virtualwalletproject.main.models.DTOs.CardDtoForPresentation;

import java.util.List;


public interface CardsService {
    boolean checkCardExistsByCardNumber(String cardNumber);
    boolean checkCardExistsById(int cardId);
    void createCard(CardDtoForCreationAndEdit card, String userName);
    void updateCard(int cardId, CardDtoForCreationAndEdit cardDto, String username);
    void deleteCard(int cardId);
    Card getCardByCardNumber(String cardNumber);
    Card getCardById(int cardId);
    CardDtoForCreationAndEdit getCardDtoForEditByCardId(int cardId, String username);
    List<Card> getCardsByUsername(String username);
    List<CardDtoForPresentation> getCardDtosForPresentationByUsername(String username);
}
