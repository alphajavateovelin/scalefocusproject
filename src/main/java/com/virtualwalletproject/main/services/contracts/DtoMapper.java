package com.virtualwalletproject.main.services.contracts;

import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.*;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.models.Wallet;

public interface DtoMapper {

    Wallet toWallet (WalletDtoForCreation wallet);
    WalletDtoForPresentation fromWalletToDtoForPresentation (Wallet wallet);

    Card toCard (CardDtoForCreationAndEdit card);
    CardDtoForCreationAndEdit fromCard (Card card);

    UserDtoForPresentationDetailed fromUserToDetailedDtoForPresentation (User user);
    UserDtoForVerification fromUserToVerificationDto (User user);
}
