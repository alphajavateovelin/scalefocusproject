package com.virtualwalletproject.main.services.contracts;

import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.PaymentResponse;

import java.io.IOException;
import java.math.BigDecimal;

public interface PaymentProcessorService {

    PaymentResponse processCardTransaction (Card card, BigDecimal amount, String idempotencyKey) throws IOException;
}
