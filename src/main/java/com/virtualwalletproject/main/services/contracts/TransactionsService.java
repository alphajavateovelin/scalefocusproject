package com.virtualwalletproject.main.services.contracts;

import com.virtualwalletproject.main.models.DTOs.TransactionFundDto;
import com.virtualwalletproject.main.models.DTOs.TransactionTransferDto;
import com.virtualwalletproject.main.models.Transaction;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.models.Wallet;
import com.virtualwalletproject.main.models.enums.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface TransactionsService {

    void createTransaction(TransactionTransferDto transactionTransferDto, String senderUsername);

    void createTransaction(TransactionFundDto transactionFundDto, String username) throws IOException;

    Page<Transaction> getAllTransactions(Pageable pageable);

    Page<Transaction> getAllTransactionsByUsername(String username, Pageable pageable);

    Page<Transaction> findAllByAmount(double amount, Pageable pageable);

    Page<Transaction> findAllByDate(Date date, Pageable pageable);

    Page<Transaction> findAllByWallet(Wallet wallet, Pageable pageable);

    Page<Transaction> findAllByType(Type type, Pageable pageable);


//    Page<Transaction> getAllByTeodor(Integer page, String startDate, String endDate, String sortAndOrder, Pageable pageable) throws ParseException;
}