package com.virtualwalletproject.main.services.PaymentProcessor;

import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.PaymentResponse;
import com.virtualwalletproject.main.models.enums.Status;
import com.virtualwalletproject.main.services.contracts.PaymentProcessorService;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.*;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;

@Service
public class PaymentProcessorServiceImpl implements PaymentProcessorService {

    @Override
    public PaymentResponse processCardTransaction(Card card, BigDecimal amount, String idempotencyKey) throws IOException {
        URL url = new URL("http://127.0.0.1:8081/payment");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json; utf-8");
        connection.setRequestProperty("x-api-key", "testKey");
        connection.setRequestProperty("Accept", "application/json");
        connection.setConnectTimeout(10000);
        connection.setReadTimeout(10000);

        connection.setDoOutput(true);
        String jsonInputString = "{\n" +
                "  \"amount\": " + amount.setScale(0, RoundingMode.DOWN) + ",\n" +
                "  \"currency\": \"" + card.getCurrency().getShortCode() +"\",\n" +
                "  \"description\": \"Add funds to virtual wallet\",\n" +
                "  \"idempotencyKey\": \"" + idempotencyKey + "\",\n" +
                "  \"cardDetails\": {\n" +
                "    \"cardNumber\": \"" + card.getCardNumber() + "\",\n" +
                "    \"expirationDate\": \"" + card.getExpirationDate() + "\",\n" +
                "    \"cardholderName\": \"" + card.getCardHolder() + "\",\n" +
                "    \"csv\": \"" + card.getCvv() + "\"\n" +
                "  }\n" +
                "}";

        try(OutputStream os = connection.getOutputStream()) {
            byte[] input = jsonInputString.getBytes();
            os.write(input, 0, input.length);
        }

        String message;
        Status status;
        int connectionStatus = connection.getResponseCode();

        InputStreamReader streamReader;
        if (connectionStatus > 299) {
            streamReader = new InputStreamReader(connection.getErrorStream());
        } else {
            streamReader = new InputStreamReader(connection.getInputStream());
        }

        BufferedReader br = new BufferedReader(streamReader);
        StringBuilder response = new StringBuilder();
        String responseLine;
        while ((responseLine = br.readLine()) != null) {
            response.append(responseLine.trim());
        }
        br.close();

        System.out.println(response.toString());
        JSONObject jsonObject = new JSONObject();
        if(!response.toString().isEmpty()) {
            jsonObject = new JSONObject(response.toString());
        }

        switch (connectionStatus) {
            case 200:
                status = Status.SUCCESSFUL;
                message = "";
                break;
            case 403:
                status = Status.FAILED;
                message = jsonObject.getString("error");
                break;
            case 400:
                status = Status.TERMINATED;
                message = jsonObject.getJSONArray("errors").get(0).toString();
                break;
            case 401:
                status = Status.TERMINATED;
                message = jsonObject.getString("error");
                break;
            default:
                status = Status.TERMINATED;
                message = "unknown error from card processing API";
                break;
        }
        connection.disconnect();

        return new PaymentResponse(status, message);
    }
}
