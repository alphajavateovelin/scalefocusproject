package com.virtualwalletproject.main.repositories;

import com.virtualwalletproject.main.models.Wallet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface WalletsRepository extends JpaRepository<Wallet, Integer> {
    Wallet getById(int id);
    Wallet getByName(String username);
    boolean existsByName(String walletName);

    @Query(value = "SELECT * FROM wallets join users_wallets uw on wallets.id = uw.Wallets_id where username like :keyword", nativeQuery = true)
    List<Wallet> getWalletsByUser (@Param("keyword") String username); //TODO this is probably not correct
}
