package com.virtualwalletproject.main.repositories;

import com.virtualwalletproject.main.models.DTOs.UserDtoForPresentationBrief;
import com.virtualwalletproject.main.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface UsersRepository extends JpaRepository<User, String> {
    User getByUsername(String username);
    boolean existsByUsername(String username);
    Page<User> findAllByBlockedStatusIsFalseAndVerifiedStatusIsTrue(Pageable pageable);
    Page<User> findAllBy(Pageable pageable);
    Page<User> findAllByVerifiedStatusIsFalse(Pageable pageable);
    Page<User> findAll(Pageable pageable);
    boolean existsByEmail(String email);
    boolean existsByPhoneNumber(String phoneNumber);

    @Query(value = "SELECT username FROM users where username like %:keyword%", nativeQuery = true)
    List<String> searchForUsersByUsername (@Param("keyword") String keyword);


    @Query("select u from User u where u.username like %?1% or u.email like %?2% or u.phoneNumber like %?3%")
    Page<User> getAllUserWithQuery(String username, String email, String phoneNumber, Pageable pageable);
}
