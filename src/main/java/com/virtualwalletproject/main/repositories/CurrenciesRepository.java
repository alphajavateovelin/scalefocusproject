package com.virtualwalletproject.main.repositories;

import com.virtualwalletproject.main.models.Currency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CurrenciesRepository extends JpaRepository<Currency, Integer> {
    List<Currency> findAllBy ();
    Currency getById (int id);
}
