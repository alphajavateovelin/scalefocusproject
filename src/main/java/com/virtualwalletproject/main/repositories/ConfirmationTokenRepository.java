package com.virtualwalletproject.main.repositories;

import com.virtualwalletproject.main.models.ConfirmationToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfirmationTokenRepository extends CrudRepository<ConfirmationToken, String> {
    ConfirmationToken findByConfirmationToken(String confirmationToken);
    boolean existsByConfirmationToken (String confirmationToken);
}
