package com.virtualwalletproject.main.repositories;

import com.virtualwalletproject.main.models.Transaction;
import com.virtualwalletproject.main.models.Wallet;
import com.virtualwalletproject.main.models.enums.Type;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Date;
@Repository
public interface TransactionsRepository extends JpaRepository<Transaction, Integer> {

    Page<Transaction> findAll(Pageable pageable);
    Page<Transaction> getAllByWallet(Wallet wallet, Pageable pageable);
    Page<Transaction> findAllByAmount(double amount, Pageable pageable);
    Page<Transaction> findAllByDate(Date date, Pageable pageable);
    Page<Transaction> findAllByWallet(Wallet wallet, Pageable pageable);
    Page<Transaction> findAllByType(Type type, Pageable pageable);
    Page<Transaction> findAllByUserUsername(String username, Pageable pageable);

    @Query("select t from Transaction t where t.date>?1 and t.date<?2")
    Page<Transaction> getAllFromTeodor(LocalDateTime start, LocalDateTime end, Pageable pageable);


//    @Query("select t from Transaction t where t.date>?1 and t.date<?2 and t.sender.username like %?3% and receiver.username like %?4%")


    //TODO custom implementation
    //TODO repository that works with both wallets and transactions
    //TODO or rollback
    //TODO or medium layer that calls two sql transactions - one for wallets and one for transactions
}
