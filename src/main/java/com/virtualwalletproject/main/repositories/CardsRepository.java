package com.virtualwalletproject.main.repositories;

import com.virtualwalletproject.main.models.Card;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CardsRepository extends JpaRepository<Card, Integer> {

    @Query(value = "select * from cards where isDeleted = 0 and cardNumber like :keyword", nativeQuery = true)
    Card getCardByCardNumberAndDeletedIsFalse (@Param("keyword") String cardNumber);

    @Query(value = "select * from cards where isDeleted = 0 and id like :keyword", nativeQuery = true)
    Card getCardByIdAndDeletedIsFalse (@Param("keyword") int cardId);

    @Query(value = "select * from cards where isDeleted = 0 and username like :keyword", nativeQuery = true)
    List<Card> findAllByUserUsernameAndDeletedIsFalse (@Param("keyword") String username);
}
