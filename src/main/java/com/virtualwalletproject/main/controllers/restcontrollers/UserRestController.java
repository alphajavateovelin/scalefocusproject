package com.virtualwalletproject.main.controllers.restcontrollers;

import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.CardDtoForCreationAndEdit;
import com.virtualwalletproject.main.models.DTOs.UserDtoForCreation;
import com.virtualwalletproject.main.models.DTOs.UserDtoForEdit;
import com.virtualwalletproject.main.models.DTOs.UserDtoForPresentationDetailed;
import com.virtualwalletproject.main.models.Transaction;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.services.contracts.CardsService;
import com.virtualwalletproject.main.services.implementation.EmailSenderService;
import com.virtualwalletproject.main.services.contracts.TransactionsService;
import com.virtualwalletproject.main.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    private UsersService usersService;
    private CardsService cardsService;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private EmailSenderService emailSenderService;
    private TransactionsService transactionsService;

    @Autowired
    public UserRestController(UsersService usersService, CardsService cardsService, UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, EmailSenderService emailSenderService, TransactionsService transactionsService) {
        this.usersService = usersService;
        this.cardsService = cardsService;
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.emailSenderService = emailSenderService;
        this.transactionsService = transactionsService;
    }

    @GetMapping()
    public Page<User> getAll(Pageable pageable) {
        return usersService.getAllUsers(pageable);
    }

    @GetMapping("/{username}")
    public UserDtoForPresentationDetailed getUserByUserName(@PathVariable String username) {
        try {
            return usersService.getDetailedUserDtoForPresentationByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create")
    public UserDtoForPresentationDetailed createUser(@RequestBody @Valid UserDtoForCreation user) {
        try {
            usersService.createUser(user);
            return usersService.getDetailedUserDtoForPresentationByUsername(user.getUsername());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/edit/{username}")
    public UserDtoForPresentationDetailed editUser(@PathVariable String username, @RequestBody @Valid UserDtoForEdit newUser) {
        try {
            usersService.updateUser(username, newUser);
            return usersService.getDetailedUserDtoForPresentationByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/delete/{username}")
    public void deleteUser(@PathVariable String username) {
        try {
            User userToBeDeleted = usersService.getUserByUsername(username);
            usersService.deleteUser(userToBeDeleted);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/add-card/{userName}")
    public void addCard(@PathVariable String userName, @RequestBody @Valid CardDtoForCreationAndEdit card) {
        try {
            cardsService.createCard(card, userName);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/edit-card/{username}/{cardId}")
    public void editCard(@PathVariable int cardId, @PathVariable String username, @RequestBody @Valid CardDtoForCreationAndEdit card) {
        try {
            cardsService.updateCard(cardId, card, username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/delete-card/{cardId}")
    public void deleteCard(@PathVariable int cardId) {
        try {
            cardsService.deleteCard(cardId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @GetMapping("/user/transaction/{username}")
    public Page<Transaction> showTransaction(@PathVariable String username, Pageable pageable) {
        try {
            return transactionsService.getAllTransactionsByUsername(username, pageable);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/list")
    public Page<User> getAllUsers(@RequestParam(name = "page") Integer page,
                                         @RequestParam(name = "input", defaultValue = "") String input) {

        return usersService.getAllByQuery(input, input, input, PageRequest.of(page, 5));
    }

//    @GetMapping("/listOfTransactions")
//    public Page<Transaction> getAllTransactionsTeodor(@RequestParam(name = "page") Integer page,
//                                                @RequestParam(name = "startDate", defaultValue = "01/01/2020") String startDate,
//                                                @RequestParam(name = "endDate", defaultValue = "01/01/2021") String endDate,
//                                                @RequestParam(name = "order", defaultValue = "") String order) throws ParseException {
//
//        return transactionsService.getAllByTeodor(page, startDate, endDate, order, PageRequest.of(page, 5));
//
//    }
}
