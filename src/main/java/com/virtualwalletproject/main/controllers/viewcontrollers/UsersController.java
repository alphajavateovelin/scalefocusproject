package com.virtualwalletproject.main.controllers.viewcontrollers;

import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.DTOs.UserDtoForEdit;
import com.virtualwalletproject.main.models.DTOs.UserDtoForVerification;
import com.virtualwalletproject.main.services.contracts.CardsService;
import com.virtualwalletproject.main.services.contracts.TransactionsService;
import com.virtualwalletproject.main.services.contracts.UsersService;
import com.virtualwalletproject.main.services.contracts.WalletsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;


@Controller
public class UsersController {
    private CardsService cardsService;
    private WalletsService walletsService;
    private TransactionsService transactionsService;
    private UsersService usersService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UsersController(CardsService cardsService, WalletsService walletsService, TransactionsService transactionsService, UsersService usersService, PasswordEncoder passwordEncoder) {
        this.cardsService = cardsService;
        this.walletsService = walletsService;
        this.transactionsService = transactionsService;
        this.usersService = usersService;
        this.passwordEncoder = passwordEncoder;
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/user")
    public String showUser(Model model, Principal principal) {
        try {
            model.addAttribute("user", usersService.getDetailedUserDtoForPresentationByUsername(principal.getName()));
            return "profile-page-2";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/user/edit")
    public String editUserForm(Model model, Principal principal) {
        try {
            model.addAttribute("userView", usersService.getDetailedUserDtoForPresentationByUsername(principal.getName()));
            model.addAttribute("userNew", new UserDtoForEdit());
            return "edit-user";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN, USER"})
    @PostMapping("/user/edit")
    public String editUser(@ModelAttribute @Valid UserDtoForEdit userNew, BindingResult errors, Model model, Principal principal) {
        if (errors.hasErrors()) {
            model.addAttribute("userView", usersService.getDetailedUserDtoForPresentationByUsername(principal.getName()));
            model.addAttribute("userNew", new UserDtoForEdit());
            model.addAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "edit-user";
        }
        try {
            usersService.updateUser(principal.getName(), userNew);
            return "redirect:/user";
        } catch (DuplicateEntityException e) {
            model.addAttribute("userView", usersService.getDetailedUserDtoForPresentationByUsername(principal.getName()));
            model.addAttribute("userNew", new UserDtoForEdit());
            model.addAttribute("error", e.getMessage());
            return "edit-user";
        }
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/user/verification")
    public String getViewUserValidation(Model model, Principal principal) {
        try {
            model.addAttribute("user", usersService.getUserDtoForVerificationByUsername(principal.getName()));
            return "verification-photos";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

    }

    @Secured({"ADMIN, USER"})
    @PostMapping("/user/verification")
    public String submitUserPhotosForVerification(Model model, @ModelAttribute(name = "user") UserDtoForVerification user, @RequestParam("selfie") MultipartFile selfiePhoto, @RequestParam("personalIdCard") MultipartFile personalIdCardPhoto, Principal principal) throws IOException {
        model.addAttribute("user", user);
        user.setSelfiePhoto(Base64.getEncoder().encodeToString(selfiePhoto.getBytes()));

        user.setPersonalIdCardPhoto(Base64.getEncoder().encodeToString(personalIdCardPhoto.getBytes()));

        usersService.updateUser(principal.getName(), user);

        return "redirect:/user";
    }


    @Secured({"ADMIN, USER"})
    @GetMapping("/user/transactions")
    public String showUserTransactions(Model model, Principal principal, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "ASC") Sort.Direction direction, @RequestParam(defaultValue = "date") String criteria) {
        try {
            Pageable pageable = PageRequest.of(page, size, Sort.by(direction, criteria));
            model.addAttribute("direction", direction);
            model.addAttribute("pageable", pageable);
            model.addAttribute("criteria", criteria);
            model.addAttribute("transactions", transactionsService.getAllTransactionsByUsername(principal.getName(), pageable));
            return "view-transactions";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/user/cards")
    public String getUserCards(Model model, Principal principal) {
        try {
            model.addAttribute("cards", cardsService.getCardsByUsername(principal.getName()));
            return "user-cards";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/user/wallets")
    public String getUserWallets(Model model, Principal principal) {
        try {
            model.addAttribute("wallets", walletsService.getWalletsByUsername(principal.getName()));
            return "user-wallets";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/search")
    @ResponseBody
    public List<String> search(HttpServletRequest request) {
        return usersService.search(request.getParameter("term"));
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/test/users")
    public String findUsers() {
        return "search-for-users";
    }
}


