package com.virtualwalletproject.main.controllers.viewcontrollers;

import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.exceptions.ForbiddenActionException;
import com.virtualwalletproject.main.exceptions.InsufficientBalanceException;
import com.virtualwalletproject.main.models.Currency;
import com.virtualwalletproject.main.models.DTOs.TransactionFundDto;
import com.virtualwalletproject.main.models.DTOs.TransactionTransferDto;
import com.virtualwalletproject.main.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import com.virtualwalletproject.main.models.User;
import org.springframework.web.server.ResponseStatusException;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

@Controller
public class TransactionsController {
    private TransactionsService transactionsService;
    private UsersService usersService;
    private CurrenciesService currenciesService;
    private WalletsService walletsService;
    private CardsService cardsService;

    @Autowired
    public TransactionsController(TransactionsService transactionsService, UsersService usersService, CurrenciesService currenciesService, WalletsService walletsService, CardsService cardsService) {
        this.transactionsService = transactionsService;
        this.usersService = usersService;
        this.currenciesService = currenciesService;
        this.walletsService = walletsService;
        this.cardsService = cardsService;
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/transactions/new")
    public String createTransactionForm(Model model, Principal principal) {
        try {
            User user = usersService.getUserByUsername(principal.getName());
            if (user.isBlockedStatus()) {
                return "user-blocked";
            }
            if (!user.isVerifiedStatus()) {
                return "user-unverified";
            }
            if (!user.isConfirmed()) {
                return "user-not-confirmed";
            }
            model.addAttribute("wallets", walletsService.getWalletsByUsername(principal.getName()));
            model.addAttribute("transactionDto", new TransactionTransferDto());
            return "create-transaction";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Secured({"ADMIN, USER"})
    @PostMapping("/transactions/new")
    public String createTransaction(@ModelAttribute("transactionDto") @Valid TransactionTransferDto transactionDto, Principal principal) {
            try {
                transactionsService.createTransaction(transactionDto, principal.getName());
                return "redirect:/transaction/success";
            } catch (EntityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            } catch (ForbiddenActionException e) {
                throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
            }
    }

    @Secured({"ADMIN, USER"})
    @GetMapping("/transactions/fund")
    public String fundForm(Model model, Principal principal) {
        try {
            User user = usersService.getUserByUsername(principal.getName());
            if (user.isBlockedStatus()) {
                return "user-blocked";
            }
            if (!user.isVerifiedStatus()) {
                return "user-unverified";
            }
            if (!user.isConfirmed()) {
                return "user-not-confirmed";
            }
            //TODO extract the access logic in separate class?
            model.addAttribute("wallets", walletsService.getWalletsByUsername(principal.getName()));
            model.addAttribute("cards", cardsService.getCardsByUsername(principal.getName()));
            model.addAttribute("transactionDto", new TransactionFundDto());
            return "add-funds-to-wallet";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @Secured({"ADMIN, USER"})
    @PostMapping("/transactions/fund")
    public String fund(@ModelAttribute("transactionDto") TransactionFundDto transactionDto, Principal principal) throws IOException {
        try {
            transactionsService.createTransaction(transactionDto, principal.getName());
            return "redirect:/transaction/success";
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
        catch (ForbiddenActionException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        catch (InsufficientBalanceException e) {
            return "redirect:/transaction/fail";
        }
    }
    @Secured({"ADMIN, USER"})
    @ModelAttribute("currencies")
    public List<Currency> populateStyles() {
        return currenciesService.getAllCurrencies();
    }
}
