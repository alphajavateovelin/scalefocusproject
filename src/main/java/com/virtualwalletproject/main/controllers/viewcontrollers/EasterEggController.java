package com.virtualwalletproject.main.controllers.viewcontrollers;


import com.virtualwalletproject.main.models.EasterEgg;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class EasterEggController {
    @GetMapping("/easter-egg")
    public String showEasterEgg(Model model) {
        model.addAttribute("egg", new EasterEgg());
        return "easter-egg";
    }
    @PostMapping("/easter-egg")
    public String returnEasterEgg(@ModelAttribute EasterEgg egg) {

        if (egg.answer.equals("ekopak")) {
            return "redirect:/easter-egg-show";
        } else return "easter-egg";
    }
    @GetMapping("/easter-egg-show")
    public String returnEasterEggPage(){
        return "easter-egg-show";
    }
}
