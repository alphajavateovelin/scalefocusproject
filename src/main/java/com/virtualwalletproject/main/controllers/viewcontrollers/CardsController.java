package com.virtualwalletproject.main.controllers.viewcontrollers;

import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.exceptions.ForbiddenActionException;
import com.virtualwalletproject.main.models.Card;
import com.virtualwalletproject.main.models.DTOs.CardDtoForCreationAndEdit;
import com.virtualwalletproject.main.services.contracts.CardsService;
import com.virtualwalletproject.main.services.contracts.CurrenciesService;
import com.virtualwalletproject.main.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;

@Controller
public class CardsController {
    private UsersService usersService;
    private CardsService cardsService;
    private CurrenciesService currenciesService;

    @Autowired
    public CardsController(UsersService usersService, CardsService cardsService, CurrenciesService currenciesService) {
        this.usersService = usersService;
        this.cardsService = cardsService;
        this.currenciesService = currenciesService;
    }
    @Secured({"ADMIN, USER"})
    @GetMapping("/cards/new")
    public String createCardForm(Model model) {
        model.addAttribute("card", new CardDtoForCreationAndEdit());
        model.addAttribute("currencies", currenciesService.getAllCurrencies());

        return "create-cards";
    }
    @Secured({"ADMIN, USER"})
    @PostMapping("/cards/new")
    public String createCard(@ModelAttribute("card") CardDtoForCreationAndEdit newCard, Principal principal, BindingResult bindingResult, Model model) {
        try {
            if (bindingResult.hasErrors()) {
                model.addAttribute("error", bindingResult.toString());
                return "create-cards";
            }
            cardsService.createCard(newCard, principal.getName());
            return "card-creation-confirmation";
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
    @Secured({"ADMIN, USER"})
    @GetMapping("/cards/{id}/edit")
    public String getSingleCardEdit (Model model, @PathVariable(name="id") int cardId, Principal principal) {
        try {
            model.addAttribute("card", cardsService.getCardDtoForEditByCardId(cardId, principal.getName()));
            model.addAttribute("currencies", currenciesService.getAllCurrencies());
            return "edit_cards";
        } catch (ForbiddenActionException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @Secured({"ADMIN, USER"})
    @PostMapping("/cards/{id}/edit")
    public String editSingleCardEdit(@Valid @ModelAttribute CardDtoForCreationAndEdit card, Principal principal, BindingResult errors, Model model, @PathVariable(name="id") int cardId ){
        try {
            if (errors.hasErrors()) {
                model.addAttribute("error", errors.toString());
                model.addAttribute("card", cardsService.getCardDtoForEditByCardId(cardId, principal.getName()));
                model.addAttribute("currencies", currenciesService.getAllCurrencies());
                return "edit_cards";
            }
            cardsService.updateCard(cardId, card, principal.getName());
            return "redirect:/user/cards";
        }
        catch (ForbiddenActionException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}

