package com.virtualwalletproject.main.controllers.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }
    @GetMapping("/adminLogin")
    public String showAdminLogin(){
        return "admin_login";
    }
}
