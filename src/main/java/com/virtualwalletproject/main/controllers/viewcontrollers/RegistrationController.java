package com.virtualwalletproject.main.controllers.viewcontrollers;

import com.virtualwalletproject.main.constants.AppConstants;
import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.models.DTOs.UserDtoForCreation;
import com.virtualwalletproject.main.repositories.ConfirmationTokenRepository;
import com.virtualwalletproject.main.services.implementation.EmailSenderService;
import com.virtualwalletproject.main.services.contracts.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


@Controller
public class RegistrationController {
    private UsersService usersService;
    private PasswordEncoder passwordEncoder;
    private UserDetailsManager userDetailsManager;
    private EmailSenderService emailSenderService;
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    public RegistrationController(ConfirmationTokenRepository confirmationTokenRepository, EmailSenderService emailSenderService, UsersService usersService, PasswordEncoder passwordEncoder, UserDetailsManager userDetailsManager) {
        this.usersService = usersService;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;
        this.emailSenderService = emailSenderService;
        this.confirmationTokenRepository = confirmationTokenRepository;
    }


    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new UserDtoForCreation());
        return "registration";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute("user") UserDtoForCreation user, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            return "registration";
        }
        try {

            usersService.createUser(user);
            return "register-confirmation";
        }
        catch (DuplicateEntityException e){
            model.addAttribute("error", e.getMessage());
            model.addAttribute("user", new UserDtoForCreation());
            return "registration";
        }
    }

    @RequestMapping(value ="/confirm-account", method = {RequestMethod.GET, RequestMethod.POST})
    public String confirmUserAccount(Model model, @RequestParam String token){
        if(usersService.confirmUser(token)){
            return "registration-confirmed";
        }
        else {
            model.addAttribute("error", "The link is invalid or broken"); //TODO need separate html to display this message
            return "error";
        }

    }

}

