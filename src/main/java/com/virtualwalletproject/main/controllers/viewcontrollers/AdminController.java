package com.virtualwalletproject.main.controllers.viewcontrollers;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.User;
import com.virtualwalletproject.main.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;

import java.security.Principal;

@Controller
public class AdminController {
    private TransactionsService transactionsService;
    private UsersService usersService;
    private AdminService adminService;
    private CardsService cardsService;
    private WalletsService walletsService;

    @Autowired
    public AdminController(AdminService adminService, TransactionsService transactionsService, UsersService usersService, CardsService cardsService, WalletsService walletsService) {
        this.transactionsService = transactionsService;
        this.usersService = usersService;
        this.adminService = adminService;
        this.cardsService = cardsService;
        this.walletsService = walletsService;
    }

    @Secured({"ADMIN"})
    @GetMapping("/admin")
    public String showUser(Model model, Principal principal){
        model.addAttribute("admin", usersService.getDetailedUserDtoForPresentationByUsername(principal.getName()));
        return "admin-profile-page";
    }

    @Secured({"ADMIN"})
    @GetMapping("/admin/transactions")
    public String getAdminTransactions(Model model, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5")int size, @RequestParam(defaultValue = "date") String criteria,  @RequestParam(defaultValue = "ASC")Sort.Direction direction){
        Pageable pageable = PageRequest.of(page, size, Sort.by(direction, criteria));
        model.addAttribute("direction", direction);
        model.addAttribute("pageable", pageable);
        model.addAttribute("criteria", criteria);
        model.addAttribute("transactions", transactionsService.getAllTransactions(pageable)); //TODO do we need DTO?
        return "view-transactions";
    }

    @Secured({"ADMIN"})
    @GetMapping("/admin/users")
    public String getAdminAllUsers(Model model, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size, @RequestParam(defaultValue = "ASC")Sort.Direction direction) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(direction,"username"));
        Page<User> userPage = usersService.getAllUserDtosForPresentationBrief(pageable);
        model.addAttribute("direction", direction);
        model.addAttribute("pageable", pageable);
        model.addAttribute("users", userPage);
        return "view-users";
    }

    @Secured({"ADMIN"})
    @GetMapping("/user/{username}/cards")
    public String getUserCards(Model model, @PathVariable String username) {
        try {
            model.addAttribute("cards", cardsService.getCardsByUsername(username));
            return "user-cards";
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN"})
    @GetMapping("/user/{username}/wallets")
    public String getUserWallets(Model model, @PathVariable String username) {
        try {
            model.addAttribute("wallets", walletsService.getWalletsByUsername(username));
            return "user-wallets";
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN"})
    @GetMapping("/user/{username}/transactions")
    public String getUserTransactions(Model model, @RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "5") int size, @PathVariable String username, @RequestParam(defaultValue = "ASC")Sort.Direction direction, @RequestParam(defaultValue = "date") String criteria) {
        try {
            Pageable pageable = PageRequest.of(page, size, Sort.by(direction, criteria));
            model.addAttribute("direction", direction);
            model.addAttribute("pageable", pageable);
            model.addAttribute("criteria", criteria);
            model.addAttribute("transactions", transactionsService.getAllTransactionsByUsername(username, pageable));
            return "view-transactions";
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN"})
    @PostMapping("admin/users/block/{userName}")
    public String blockUser(@PathVariable String userName){
        adminService.block(userName); //TODO separate block and unblock
        return "redirect:/admin/users";
    }

    @Secured({"ADMIN"})
    @GetMapping("admin/users/verification")
    public String viewUsersForVerification(Model model, Pageable pageable){
        model.addAttribute("users", usersService.getAllUnverifiedUsers(pageable)); //TODO need DTO
        return "view-users-verification";
    }

    @Secured({"ADMIN"})
    @GetMapping("admin/users/verify/{userName}")
    public String showUserForVerification(@PathVariable String userName, Model model) {
        try {
            model.addAttribute("user", usersService.getUserByUsername(userName));
            return "view-user-for-verification";
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Secured({"ADMIN"})
    @PostMapping("admin/users/verify/{userName}")
    public String verifyUser(@PathVariable String userName){
        adminService.verify(userName);
        return "redirect:/admin/users/verification";
    }
}
