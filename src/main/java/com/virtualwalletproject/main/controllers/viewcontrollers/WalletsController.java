package com.virtualwalletproject.main.controllers.viewcontrollers;

import com.virtualwalletproject.main.exceptions.DuplicateEntityException;
import com.virtualwalletproject.main.exceptions.EntityNotFoundException;
import com.virtualwalletproject.main.models.Currency;
import com.virtualwalletproject.main.services.contracts.DtoMapper;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForCreation;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForEdit;
import com.virtualwalletproject.main.models.DTOs.WalletDtoForPresentation;
import com.virtualwalletproject.main.models.Wallet;
import com.virtualwalletproject.main.services.contracts.CurrenciesService;
import com.virtualwalletproject.main.services.contracts.UsersService;
import com.virtualwalletproject.main.services.contracts.WalletsService;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
public class WalletsController {
    private DtoMapper dtoMapper;
    private WalletsService walletsService;
    private CurrenciesService currenciesService;
    private UsersService usersService;

    public WalletsController(DtoMapper dtoMapper, WalletsService walletsService, CurrenciesService currenciesService, UsersService usersService) {
        this.dtoMapper = dtoMapper;
        this.walletsService = walletsService;
        this.currenciesService = currenciesService;
        this.usersService = usersService;
    }
    @Secured({"ADMIN, USER"})
    @GetMapping("/wallets/new")
    public String createWalletForm (Model model) {
        model.addAttribute("newWallet", new WalletDtoForCreation());
        return "create_wallet";
    }
    @Secured({"ADMIN, USER"})
    @PostMapping("/wallets/new")
    public String createWallet (@ModelAttribute @Valid WalletDtoForCreation newWallet, Principal principal) {
        try {
            walletsService.createWallet(newWallet, principal.getName());
            return "wallet-creation-confirmation";
        }
        catch (DuplicateEntityException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
    @Secured({"ADMIN, USER"})
    @GetMapping("/wallets/{id}/edit")
    public String getSingleWallet(Model model, @PathVariable(name="id") int walletId){
        try {
            Wallet wallet = walletsService.getWalletById(walletId);
            WalletDtoForPresentation walletDto = dtoMapper.fromWalletToDtoForPresentation(wallet);
            model.addAttribute("wallet", walletDto);
            return "edit_wallet";
        }
        catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @Secured({"ADMIN, USER"})
    @PostMapping("/wallets/{id}/edit")
    public String editSingleWallet(Principal principal, @ModelAttribute @Valid WalletDtoForEdit wallet, BindingResult errors, Model model, @PathVariable(name = "id") int walletId){
        if (errors.hasErrors()){
            model.addAttribute("error", errors.toString());
            return "edit_wallet";
        }
        walletsService.updateWallet(wallet, walletId);
        return String.format("redirect:/user/%s/wallets", principal.getName());

    }

    @Secured({"ADMIN, USER"})
    @ModelAttribute("currencies")
    public List<Currency> populateStyles() {
        return currenciesService.getAllCurrencies();
    }
}
