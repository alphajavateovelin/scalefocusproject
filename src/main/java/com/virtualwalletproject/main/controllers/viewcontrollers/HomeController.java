package com.virtualwalletproject.main.controllers.viewcontrollers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller("/")
public class HomeController {

    @GetMapping
    public String showHomePage(){
        return "home-default-5";
    }

    @GetMapping("/about-us")
    public String showAboutUs(){
        return "page-about-us-3";
    }
    @GetMapping("/contact-us")
    public String showContactUs(){
        return "page-contact-2";
    }
    @GetMapping("/transaction/success")
    public String showTransactionSuccess(){
        return "Transaction-success";
    }
    @GetMapping("/transaction/fail")
    public String showTransactionFail(){
        return "Transaction-fail";
    }
}